# PiM-SK-SS

Prior-incorporated MMPC Skeleton-based Stochastic Search

implemented as part of master thesis: 

*Inference of interaction networks from
multi-omics data using Bayesian networks*

Author:
Alikhan Anuarbekov, 

Supervisor: 
doc. Ing. Jiri Klema, Ph.D.

CTU FEE,
Department of Computer Science,
2023

# Usage
Navigate to [workflow.Rmd](workflow.Rmd) file for a complete overview of every part of the source code functionality
