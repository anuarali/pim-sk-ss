prev_wd <- getwd()
#setwd(getSrcDirectory(function(){})[1])

this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

source("BiDAG_BGe.R")
Rcpp::sourceCpp("skeleton.cpp")

BGe_compute <- function(adj, param)
{
  nodes_cand <- colnames(adj)
  sc <- sum(sapply(nodes_cand, function(node)
  {
    neights <- names(which(adj[,node] > 0))
    n <- param$n
    score <- BGe_node_compute(node, neights, param, n)
    return(score)
  })
  )
  #cat("Score of part:", sc)
  return(sc)
}
setwd(prev_wd)
remove(prev_wd)


BGe_change <- function(part_id, from, to, action_is_add, score_prev, G_adjs, param)
{
  adj <- G_adjs[[part_id]]

  node <- colnames(adj)[to]
  neights <- names(which(adj[,node] > 0))
  n <- param$n
  score_node_prev <- BGe_node_compute(node, neights, param, n)
  
  adj[from, to] <- action_is_add
  
  node <- colnames(adj)[to]
  neights <- names(which(adj[,node] > 0))
  score_node <- BGe_node_compute(node, neights, param, n)
  
  adj[from, to] <- (1 - action_is_add)
  
  diff <- score_node - score_node_prev
  ret <- list(score=diff + score_prev, diff=diff)
  
  return(ret)
}





#'
#' Compute a score~proportional to~ log P(G) for a given graph. This part of function should be called 
#'
#' For an integral formula of the Bayesian prior see:
#' https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3957076/
#' 
#' The formula used below is the closed-form solution
#'  for definite integral with the given Beta_L, Beta_H.
#' 
#' @param E A previously computed energy U(G) for a graph
#' @param N A total number of genes
#' @param beta_L A lower threshold on prior's weight      (numeric) 
#' @param beta_H An upper threshold on prior's weight     (numeric)
#' @returns score ~proportional to~ P(G), network prior for a given network
#'
pP_G_compute <- function(E, N_sq, beta_L, beta_H)
{
  U <- E / (N_sq)
  delta_beta <- beta_H - beta_L
  
  ret <- exp(-U * beta_L) - exp(-U * beta_H)
  ret <- ret / delta_beta
  ret <- ret / U
  ret <- log(ret)
  return(ret)
}

P_G_change <- function(part_id, from, to, action_is_add, energies, scores, regs, B_PKs, beta_L, beta_H, adj)
{
  energy_prev <- energies[part_id]
  score_prev <- scores[part_id]
  
  
  B_PK_mat <- B_PKs[[part_id]]
  N_sq <- ncol(B_PK_mat) * nrow(B_PK_mat)
  b_i_j <- B_PK_mat[from, to]
  
  
  # IntOMICS change:
  # if edge included ----> (1 - b_i_j)
  # if edge no included -> (b_i_j)
  # difference:         
  #     remove --------->  +(1 - 2 * b_i_j)
  #     add    --------->  -(1 - 2 * b_i_j)
  u_change <- 1 - 2 * b_i_j
  u_change <- (1 - 2 * action_is_add) * u_change
  
  
  
  # Isci change
  # remove -> + b_i_j
  # add    -> - b_i_j
  u_change <- (1 - 2 * action_is_add) * b_i_j
  
  U.new <- energy_prev + u_change
  score <- pP_G_compute(U.new, N_sq, beta_L, beta_H)
  
  
  
  # Regularization term
  prev.reg <- f_log_combi(nrow(adj), sum(adj[,to]))
  new.reg <- sum(adj[,to]) - (1 - 2 * action_is_add)
  new.reg <- f_log_combi(nrow(adj), new.reg)
  diff.reg <- new.reg - prev.reg

  diff <- score - score_prev + diff.reg
  energies[part_id] <- U.new
  scores[part_id] <- score
  
  #regs[part_id] <- regs[part_id] + diff.reg

  ret <- list(score=sum(scores) + sum(regs), diff=diff, energy=energies, priors=scores)#, regs=regs)
  
  return(ret)
}





#'
#' Compute a score-P(G) for a given graph. Formula (by Isci):
#' E(i,j) = 
#'        (if edge in G) = 1 - B(i,j) 
#'        
#'    (if edge not in G) = B(i,j)
#' 
#' E(G) = sum(i,j) E(i,j)
#' 
#' For theoretical reference see:
#' * IntOMICS:
#' https://doi.org/10.1186/s12859-022-04891-9
#' * Bayesian prior see:
#' https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3957076/
#' 
#' @param G_adjs A list of adjacency matrices across each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param B_PKs A list of prior matrices for each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param beta_L A lower threshold on prior's weight      (numeric) 
#' @param beta_H An upper threshold on prior's weight     (numeric)
#' @returns A list with 2 values, score-P(G) and energy U(G)
#'
P_G <- function(G_adjs, B_PKs, beta_L, beta_H)
{
  scores <- c()
  energies <- c()
  regs <- c()
  
  for(i in 1:length(G_adjs)) # for each partition interaction separately
  {
    adj <- G_adjs[[i]]  
    N_sq <- nrow(adj) * ncol(adj)  # compute total number of cells
    E <- sum(1 - B_PKs[[i]] * G_adjs[[i]])
    sP_G <- pP_G_compute(E, N_sq, beta_L, beta_H)
    
    scores <- c(scores, sP_G)  # Save partial results for further
    energies <- c(energies, E)  
    
    reg_term <- colSums(adj)
    reg_term <- sapply(reg_term, function(c) f_log_combi(nrow(adj), c))
    reg_term <- sum(reg_term)
    regs <- c(regs, reg_term)
  }
  

  # energy could be potentially changed by a constant

  return(list(score=sum(scores) + sum(regs), energy=energies, scores=scores, regs=regs))
}

#'
#' Compute a P(D | G) for a given graph, e.g., BGe score without the prior part
#' 
#' @param G_adjs A list of adjacency matrices across each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param param_in A precomputed parameters for BGe computation
#' @returns P(D | G) = BGe score of a given graph
#'
P_D_G <- function(G_adjs, param_in)
{
  pDG1 <- 0
  for(i in 1:length(G_adjs)) # for each partition interaction separately
  {
    adj <- G_adjs[[i]]
    #cat(paste(i, "/", length(G_adjs), "\n"))
    pDG1 <- pDG1 + BGe_compute(adj, param_in)
  }
  
  # They are still the same!
  # nodes_cand <- rownames(G_adjs[[1]])
  # pDG2 <- sum(sapply(nodes_cand, function(node)
  # {
  #   score <- BGe_node_compute(node, integer(0), param_in)
  #   return(score)
  # })
  # )
  # + pDG2
  
  return(pDG1)
}

#'
#' Compute a score list: [log P(D|G), log P(G)] for a given graph, e.g., 
#'    log P(G | D) = log P(D|G) + log P(G) 
#'    ~ log (BGe score, data only) + log (Prior knowledge)
#'    
#' The proportionality is the absence of normalization constant, e.g.,
#' prior score is the P(G|D) multiplied by some constant
#' 
#' @param G_adjs A list of adjacency matrices across each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param B_PKs A list of prior matrices for each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param beta_L A lower threshold on prior's weight      (numeric) 
#' @param beta_H An upper threshold on prior's weight     (numeric)
#' @param param_in A precomputed parameters for BGe computation
#' @returns P(D | G) = BGe score of a given graph
#'
score_G <- function(G_adjs, B_PKs, beta_L, beta_H, param_in)
{
  pG_list <- P_G(G_adjs, B_PKs, beta_L, beta_H)
  pDG <- P_D_G(G_adjs, param_in)
  #pGD <- pG * pDG
  return(list("pG"=pG_list$score, "pDG"=pDG, "energy"=pG_list$energy, "priors"=pG_list$scores, 
              "pG_change"=0, "pDG_change"=0, "regs"=pG_list$regs))
}

#'
#' Compute a change score list: [log P(D|G), log P(G)] for a given graph, e.g., 
#'    [log P(D|G), log P(G)] after 1 action of ADD/DELETE 
#'    
#' 
#' @param G_adjs A list of adjacency matrices across each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param B_PKs A list of prior matrices for each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param beta_L A lower threshold on prior's weight      (numeric) 
#' @param beta_H An upper threshold on prior's weight     (numeric)
#' @param param_in A precomputed parameters for BGe computation
#' @param sizes A vector of each partition's size 
#' @param score_prev A previous return of score_G/score_change_G
#' @param part_id An index of G_adjs partitions
#' @param from A row index to add to G_adjs partition
#' @param to A column index to add to G_adjs partition
#' @param action_is_add A boolean defining the action, if TRUE -> add edge, if FALSE -> delete edge
#'
#'
#' @returns P(D | G) = BGe score of a given graph after change
#'
score_change_G <- function(G_adjs, B_PKs, beta_L, beta_H, param_in,
                           score_prev, part_id, from, to, action_is_add)
{
    ret <- score_prev
    adj <- G_adjs[[part_id + 1]]
    # ! Convert C++ indices to Rcpp indices(+1) !
    AC <- P_G_change(part_id+1, from+1, to+1, action_is_add, ret$energy, ret$priors, ret$regs, B_PKs, beta_L, beta_H, adj)
    B <- BGe_change(part_id+1, from+1, to+1, action_is_add, ret$pDG, G_adjs, param_in)
    
    ret <- list()
    ret$pG <- AC$score
    ret$pDG <- B$score
    ret$energy <- AC$energy
    ret$priors <- AC$priors
    ret$pG_change <- AC$diff
    ret$pDG_change <- B$diff
    ret$regs <- AC$regs
    return(ret)
}



#' Run a modified SKeleton-Based Stochastic Search. 
#' Requires an input as:
#' N = number of genes
#' S = number of samples
#' 
#' @param GE A gene expression matrix,                              (N x S)
#' @param PK A prior knowledge matrix,                              (N x N)
#' @param SK A skeleton inferred from MMPC_tripartite,              (N x N)
#' @param parts A list of partitions, total unique elements:        (N)
#' @param I A global number of iterations to perform, default=1000  (integer)
#' @param alpha A threshold prob.value for final edge cutoff        (alpha)
#' @param debug Logical, TRUE if a debugging print is needed        (logical)
#' @param beta_L A lower threshold on prior's weight, default = 0.5 (numeric) 
#' @param beta_H An upper threshold on prior's weight, default=10.5 (numeric) 
#' @param cutoff_ratio A ratio of edges that network can have,<0,1> (numeric)
#' @returns A binary matrix, the final inferred Bayesian net        (N x N)
#'
SK_SS_tripartite <- function(GE, PK, SK, parts, I=1, debug=FALSE, alpha=0.05, beta_L=0.5, beta_H=10.5, cutoff_ratio=0.1)
{
  if(missing(GE) || missing(PK) || missing(SK) || missing(parts)) {
    stop("Some of the input arguments were not provided, need: Ge, PK, SK, parts")
  }
  
  if(!(class(GE)[1] %in% c("matrix", "data.frame")))
  {
    stop("GE should be a matrix or data.frame")
  }
  if(!(class(PK)[1] %in% c("matrix", "data.frame")))
  {
    stop("PK should be a matrix or data.frame")
  }
  if(!(class(SK)[1] %in% c("matrix", "data.frame")))
  {
    stop("SK should be a matrix or data.frame")
  }
  if((class(parts) != "list") || (class(unlist(parts)) != "character"))
  {
    stop("parts should be a list of character vectors")
  }
  if(ncol(PK) != nrow(PK))
  {
    stop("PK should be a square logical matrix of size N x N, where N=number of genes")
  }
  if(sum(dim(PK) == dim(SK)) != 2)
  {
    stop("PK and SK should both have a same size square logical matrix of size N x N, where N=number of genes")
  }
  if(ncol(GE) != ncol(PK))
  {
    stop("GE and PK should have the same number of genes, GE=(S x N), PK=(N x N)")
  }
  if(length(unlist(unique(parts))) != ncol(GE))
  {
    stop("partitions should contain all genes, e.g., length(unlist(unique(parts))) == N")
  }
  
  # Remove zero rows
  zeroGenes <- names(which(colSums(GE) == 0))
  if(length(zeroGenes) > 0)
  {
    cat("Removing zero-count genes: ", zeroGenes, "\n")
    
    for(i in 1:length(parts))
    {
      prev <- parts[[i]]
      parts[[i]] <- prev[!(prev %in% zeroGenes)]
    }
  }
  
  # Extract sizes of every partitions
  sizes <- unlist(lapply(parts, length))
  cat("Partition have sizes of: ", sizes, "\n")
  
  
  # Extract every partition's GE,PK,SK parts
  PK_in <- list()
  SK_in <- list()
  
  for(i in 2:length(parts))
  {
    # PK
    subPK <- PK[parts[[i-1]],parts[[i]]]
    subPK <- as.matrix(subPK)
    
    # convert PK to prior scores as in IntOMICS/Bayesian Network Prior(Isci)
    subPK <- subPK * 0.5 + 0.5   # ---> 1 = prior,   0.5 = no prior, but allowed edge
    # all non-allowed edges are removed explicitly by list
    
    PK_in[[i-1]] <- subPK
    remove(subPK)
    # SK
    subSK <- SK[parts[[i-1]],parts[[i]]]
    subSK <- as.matrix(subSK)
    SK_in[[i-1]] <- subSK
    remove(subSK)
  }
  
  # prepare BGe score parameters
  # this is a large computation that computes a large parameter matrix:
  # N x N, where N is the sum of sizes, e.g., total number of genes
  cat("Computing large BGe TN matrix...\n")
  #myScore <- scoreparameters_BiDAG_BGe(n = ncol(GE), data = GE)
  #param_in <- list(n=myScore$n, awpN=myScore$awpN, TN=myScore$TN, scoreconstvec=myScore$scoreconstvec)
  #remove(myScore)
  #saveRDS(param_in, "param.RData")
  param_in <- readRDS("param.RData")
  cat("TN matrix have a size of: ", dim(param_in$TN), "\n")

  cat("Running the SK-SS Rcpp part:\n")
  G_out <- c_SK_SS(PK_in,SK_in, sizes, param_in,
                      I, debug, beta_L, beta_H, cutoff_ratio,
                      score_G, score_change_G)
  
  
  
  return(G_out)
}
