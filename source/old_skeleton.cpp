/**
 * This part will be the header file declarations in package, but <Rcpp::sourceCpp()> requires single file to work, so it is here for now
 */

// Pure C++
#include <numeric>
#include <bitset>
#include <string>
#include <vector>
#include <algorithm>
#include <map>

// Pure C99
#include <cstring>
#include <cmath>
#include <limits.h>
#include <cstdio>
#include <cstdint>

/**
 * We include the external library of RcppArmadillo, 
 * this allows for a future usage of linear algebra, advanced Rcpp, etc...
 * 
 * NOT USED AS FOR NOW
 */
// [[Rcpp::depends(RcppArmadillo)]]
#include <RcppArmadillo.h>
#include <RcppArmadilloExtensions/sample.h>
//#include <Rcpp.h>

/**
 * Most used Rcpp objects, used to remove Rcpp prefix
 */
using Rcpp::Rcout;
using Rcpp::Rcerr;
using Rcpp::NumericVector;
using Rcpp::NumericMatrix;
using Rcpp::DataFrame;
using Rcpp::Nullable;
using Rcpp::LogicalMatrix;
using Rcpp::IntegerMatrix;
using Rcpp::List;
using Rcpp::Function;
using Rcpp::IntegerVector;
using Rcpp::_;

/**
 * Also -- include the "saveRDS" function for partial result serialization and save
 */
Rcpp::Environment base("package:base");
Function saveRDS = base["saveRDS"];


/**
 * This part will be the separate header file for tuple + hash + Trie data structures
 */
#include <tuple>

/**
 * We will encode a 3D coordinate: 
 *   > listID --> ID of the partition of our N-partition graph, goes: (0, N-2)
 *       Note: because, for example, for 3 partition(A,B,C), we have 2 adjacency matrices: AB, BC
 *   > FROM, TO  --> row, column ID of the particular
 */
typedef std::tuple<uint8_t, uint16_t, uint16_t> key_tp;

/**
 * Hashing of the triplet. Uses the simple boost libraries' implementation of tuple hash 
 * 
 * https://stackoverflow.com/questions/7110301/generic-hash-for-tuples-in-unordered-map-unordered-set
 */
struct key_hash : public std::unary_function<key_tp, std::size_t>
{
  std::size_t operator()(const key_tp& k) const
  {
    std::size_t ret = 0;
    ret ^= std::hash<uint8_t>()(std::get<0>(k)) + 0x9e3779b9 + (ret<<6) + (ret>>2);
    ret ^= std::hash<uint16_t>()(std::get<1>(k)) + 0x9e3779b9 + (ret<<6) + (ret>>2);
    ret ^= std::hash<uint16_t>()(std::get<2>(k)) + 0x9e3779b9 + (ret<<6) + (ret>>2);
    
    return ret;
  }
};

struct key_comp
{
  /**
   * Java/C++ like comparator function:
   * 
   * Comparison is performed in the order:
   *   listID(0) -> to(2) -> from(1)
   */
  inline bool operator() (const key_tp& k1, const key_tp& k2)
  {
    if(std::get<0>(k1) != std::get<0>(k2)) return std::get<0>(k1) < std::get<0>(k2);
    if(std::get<2>(k1) != std::get<2>(k2)) return std::get<2>(k1) < std::get<2>(k2);
    return std::get<1>(k1) < std::get<1>(k2);
  }
};

void insert_sorted( std::vector<key_tp> & vec, key_tp const& item )
{
  vec.insert
  ( 
      std::upper_bound( vec.begin(), vec.end(), item, key_comp()),
      item 
  );
}
void erase_sorted(std::vector<key_tp> & vec, key_tp const& item )
{
  vec.erase(std::remove(vec.begin(), vec.end(), item), vec.end());
}


enum ChangeType
{
  ADD = true, 
  DELETE = false
};



// ########################################### COMPRESSED TRIE CODE ###############################################################
struct Node
{
  bool is_graph = false;
  key_tp *values = nullptr;
  uint32_t num_values = 0;
  
  std::unordered_map<
    key_tp,
    Node *,
    key_hash>
    next_map;
  
  void print()
  {
    std::cout << "[";
    for(uint32_t i = 0; i < num_values; i++)
    {
      key_tp k = values[i];
      std::cout << "(" 
                << (uint32_t)std::get<0>(k) << "," 
                << std::get<1>(k) << "," 
                << std::get<2>(k) << ") ";
    }
    std::cout << "]    ";
  }
};

inline static void G_change(Node* node, std::vector<LogicalMatrix*>& G_adjs, bool value)
{
  for(uint32_t i = 0; i < node->num_values; i++)
  {
    key_tp key = node->values[i];
    const uint8_t one = std::get<0>(key);
    const uint16_t two = std::get<1>(key);
    const uint16_t three = std::get<2>(key);
    (*G_adjs[one])(two, three) = value;
  }
}

/**
 * Apply the Node's key_tp triplet index as TRUE on adjacency matrix 
 */
inline static void G_forward(Node* node, std::vector<LogicalMatrix*>& G_adjs)
{   
    G_change(node, G_adjs, true);
}
/**
 * Apply the key_tp's triplet index as FALSE on adjacency matrix 
 */
inline static void G_backward(Node* node, std::vector<LogicalMatrix*>& G_adjs)
{
    G_change(node, G_adjs, false);
}


#define ALLOC_SIZE ((4UL << 13))
Node PREALLOCATED_POOL[ALLOC_SIZE];

/**
 * Optimization technique -- allocate a large portion of nodes in advance on stack,
 * so that the OS won't spend any additional time allocating nodes one by one.
 */
class NodePool
{
public:
  NodePool()
  {
  }
  
  Node *get()
  {
    if (this->current_index >= ALLOC_SIZE)
    {
      Rprintf("Maximum number of nodes (%5lu) reached! Now allocating one by one!", ALLOC_SIZE);
      Node* ret = new Node;
      return ret;
    }
    Node *ret = PREALLOCATED_POOL + this->current_index;
    this->current_index++;
    return ret;
  }
  
  uint64_t size()
  {
    return this->current_index;
  }
  
protected:
  uint64_t current_index = 0;
};

/**
 * After Trie receives a word, only the end of it is actually a subgraph,
 * 
 * Example to illustrate the intermediate subgraph mechanics:   
 *        add 2 -> add 1 -> add 5 -> add 4 -> delete 2 -> add 3
 *        
 *        then, the visited subgraphs are:
 *            {[2], [1, 2], [1, 2, 5], [1, 2, 4, 5], [1, 4, 5], [1, 3, 4, 5]}
 *        
 *        
 *        which results in Trie (Which is then compressed):
 *        
 *               2
 *        root---|      5
 *               |   2--| 
 *               1*--|  4*---5
 *                   |  
 *                   4*--5  
 *                   |
 *                   3*--4*--5
 *        
 *        
 *        ! Note -> nodes with * haven't been visited and, thus, not subgraphs !
 *        Compressed trie example:
 *        https://www.geeksforgeeks.org/compressed-tries/
 *        
 *            
 *  > if no flag is used to determine the node in Trie, we can't mark the above
 *  > when pushing a word, only its last tail node will be marked
 */
class CompressedTrie
{
public:
  CompressedTrie()  {}
  ~CompressedTrie()  {}
  
  /**
   * Append a word consisting of sorted ADD only nodes.
   * return TRUE only if non-visited subgraph added
   */
  bool AppendChain(std::vector<key_tp>& word);
  
  /**
   * Print current Trie, used only for debug purposes
   */
  void PrintTrie();
  
  /**
   * Randomly chooses a graph from the Trie and sets the matrix(adjacency) 
   * to it
   */
  void SampleAdjacencyMatrix(std::vector<LogicalMatrix*>& matrix);
  
  /**
   * Return the number of nodes in a graph
   */
  uint64_t num_nodes() {return this->node_pool.size();}
  
  /**
   * Return the number of nodes that are actually subgraphs
   */
  uint32_t num_graphs() {return this->total_num_graphs;}
  
protected:
  NodePool node_pool;
  Node *root = this->node_pool.get();
  
  uint32_t total_num_graphs = 0;

  uint32_t current_needed_index;
  uint32_t current_visited_index;
  std::vector<LogicalMatrix*>* current_G_adjs;
  void findRecursive(Node *currNode);
  
  void appendWordToNode(Node* toAppend, key_tp* word, uint32_t len);  // TODO remove, not used when no delete
  bool appendRecursive(key_tp* word, uint32_t len);
  uint32_t compareValues(Node* toAppend, key_tp* word, uint32_t len);
  Node* createNode(key_tp* word, uint32_t len);
  void subdivideNode(Node* node, uint32_t divideIndex);
  
  uint64_t SampleLong()
  {
    uint32_t sampledIndex = Rcpp::sample(this->total_num_graphs, 1)[0] - 1; 
    uint32_t sampledIndex2 = Rcpp::sample(this->total_num_graphs, 1)[0] - 1; 
    uint64_t ret = sampledIndex;
    ret = ret << 32;
    ret = ret + sampledIndex2;
    return ret;
  }
};



bool CompressedTrie::AppendChain(std::vector<key_tp>& word)
{
  bool ret = this->appendRecursive(&word[0], word.size());
  this->total_num_graphs += ret;
  return ret;
}


bool CompressedTrie::appendRecursive(key_tp* word, uint32_t len)
{
  Node* currNode = this->root;
  
  
  while(true)
  {
    key_tp currFirstLetter = word[0];
    
    // No branch exists for our word
    if(currNode->next_map.find(currFirstLetter) == currNode->next_map.end())
    {
      // append new node with entire word
      Node* appended = this->createNode(word, len);
      currNode->next_map[currFirstLetter] = appended;
      appended->is_graph = true;
      return true;
    }
    
    Node* cand = currNode->next_map[currFirstLetter];
    
    // Find an index at which graph and word divert
    uint32_t moveIndex = this->compareValues(cand, word, len);
    
    // If node isnt fully in our word -> divide aligned and unaligned part
    if(moveIndex < cand->num_values)
    {
      subdivideNode(cand, moveIndex);
    }
    // If node is our entire word -> mark and end
    if(moveIndex == len)
    {
      bool ret = cand->is_graph;
      cand->is_graph = true;
      return ret;
    }
    
    // go deeper
    currNode = cand;
    word = word + moveIndex;
    len = len - moveIndex;
  }
  
  
  
  return false;
}

#include <queue>

void CompressedTrie::PrintTrie()
{
  std::queue<Node*> inorder_q;
  inorder_q.push(this->root);
  
  std::cout << "#############################\n";
  
  while(!inorder_q.empty())
  {
    uint32_t size = inorder_q.size();
    for(int i = 0; i < size; i++)
    {
      Node* n = inorder_q.front();
      inorder_q.pop();
      n->print();
      
      for(const auto& value : n->next_map)
      {
        inorder_q.push(value.second);
      }
    }
    std::cout << "\n";
  }
}

void CompressedTrie::subdivideNode(Node* node, uint32_t divideIndex)
{
  uint32_t prevLen = node->num_values;
  if(prevLen == divideIndex) return;
  
  
  node->num_values = divideIndex;
  
  Node* newNode = this->node_pool.get();
  newNode->values = node->values + divideIndex;
  newNode->num_values = prevLen - divideIndex;
  newNode->next_map = node->next_map;
  newNode->is_graph = node->is_graph;
  
  node->next_map = {};
  node->next_map[newNode->values[0]] = newNode;
  node->is_graph = false;
  
  return;
}

uint32_t CompressedTrie::compareValues(Node* toAppend, key_tp* word, uint32_t len)
{
  uint32_t size = toAppend->num_values;
  size = (size < len) ? size : len;
  
  for(uint32_t i = 1; i < size; i++)
  {
    if(word[i] != toAppend->values[i])
    {
      return i;
    }
  }
  return size;
}

Node* CompressedTrie::createNode(key_tp* word, uint32_t len)
{
  Node* ret = this->node_pool.get();
  ret->num_values = len;
  ret->values = new key_tp[len];
  std::memcpy(ret->values, (void*)word, len * sizeof(key_tp));
  
  return ret;
}

void CompressedTrie::appendWordToNode(Node* toAppend, key_tp* word, uint32_t len)
{
  key_tp* newValues = new key_tp[toAppend->num_values + len];
  std::memcpy(newValues, toAppend->values, toAppend->num_values * sizeof(key_tp));
  std::memcpy(newValues + toAppend->num_values, word, len * sizeof(key_tp));
  delete[] toAppend->values;
  toAppend->values = newValues;
  toAppend->num_values += len;
}

void CompressedTrie::SampleAdjacencyMatrix(std::vector<LogicalMatrix*>& matrix)
{
  this->current_needed_index = this->SampleLong() % this->total_num_graphs;
  this->current_needed_index++;
  this->current_visited_index = 0;
  this->current_G_adjs = &matrix;
  
  this->findRecursive(this->root);
  
  return;
}

void CompressedTrie::findRecursive(Node *currNode)
{
  if (this->current_visited_index == this->current_needed_index)
  {
    return;
  }
  
  for (const auto &item : currNode->next_map)
  {
    const auto &key = item.first;
    const auto &value = item.second;
    
    G_forward(value, *this->current_G_adjs);
    
    this->current_visited_index += value->is_graph;
    
    this->findRecursive(value);
    
    if (this->current_visited_index == this->current_needed_index)
    {
      return;
    }
    
    G_backward(value, *this->current_G_adjs);
  }
  
  return;
}

struct key_action_tp
{
  ChangeType type;
  key_tp coord;
  
  void forward(std::vector<LogicalMatrix*>& adjs)
  {
    uint32_t listID = std::get<0>(coord);
    uint32_t from = std::get<1>(coord);
    uint32_t to = std::get<2>(coord);
    
    (*adjs[listID])(from, to) = type;
  }
};
// ########################################### COMPRESSED TRIE CODE ###############################################################


struct IData
{
  const List* PK;
  const List* SK;
  const IntegerVector* sizes;
  uint32_t N;
  const List* bge_param;
  
  Function* score_full_func;
  Function* score_change_func;
};
struct ISettings
{
  uint32_t I;
  bool debug;
  double beta_L;
  double beta_H;
  double cutoff_ratio;
};

#define SAVE_FREQ 10000
#define PRINT_FREQ 10000
#define PARTIAL_ADD_ONLY "[pSK-SS][add only]"
#define PARTIAL_ADD_REMOVE "[pSK-SS][add-remove]"
#define SKSS_PHASE "[SK-SS][add-remove]"

class Network
{
protected:
  IData input_data;
  ISettings settings;
  CompressedTrie V_G;
  
  /**
   * sort individual elements from lowest to largest by unordered_set
   * this will be used to push to TRIE -> count subgraphs
   */
  std::vector<key_tp> ord_V_G;
  
  std::vector<key_action_tp> unord_V_G;
  
  /**
   * store the current adjacency graph and the best MAP model found during the search
   *   > update only when better model is found
   */
  std::vector<LogicalMatrix*> G;
  std::vector<LogicalMatrix*> G_sampled;
  
  NumericVector visitedScores;
  NumericVector acceptedScores;
  uint32_t current_id = 0;
  
  double probabilityMaxOptim = -99999999999.0; 
  std::vector<LogicalMatrix*> G_optim;
  
  
public:  
  Network(IData input, ISettings settings)
  {
    this->input_data = input;
    this->settings = settings;

    this->initializeLists();
  }
  
  ~Network()
  {
    this->deleteLists();
  }
  
  void partialSK_SS()
  {
    for(uint32_t i = 0; i < this->settings.I; i++)
    {
      Rcout << "partialSK-SS, Iteration: " << i << "\n";
      this->resetSampledG();
      this->emptyCurrCountsAndG();
      this->partialSK_SS_iter();
    }
  }
  
  void SK_SS()
  {
    this->partialSK_SS();
    
    for(uint32_t i = 0; i < this->settings.I; i++)
    {
      Rcout << "SK-SS, Iteration: " << i << "\n";
      this->resetSampledG();
      this->V_G.SampleAdjacencyMatrix(this->G_sampled);
      
      
      this->emptyCurrCountsAndG();
      this->SK_SS_phase2_iter();
    }
  }
  
  List& result()
  {
    Rprintf("Optimum log probability: %.10f\n", this->probabilityMaxOptim);

    const List* SK_list = this->input_data.SK;
    List* ret = new List(SK_list->length());

    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      (*ret)[i] = *this->G_optim[i];
    }
    
    return *ret;
  }
  
protected:  
  void SK_SS_phase2_iter()
  {
    NumericVector twoScores = this->callScoreFull();
    const List* SK_list = this->input_data.SK;
    
    IntegerVector riter = Rcpp::sample(
      this->input_data.N, (this->input_data.N * this->settings.cutoff_ratio));
    
    saveRDS(riter, Rcpp::Named("file", "sampled_indices.RData"));
    
    
    if((twoScores[0] + twoScores[1]) > this->probabilityMaxOptim)
    {
      Rcout << "YOU IDIOT, YOU HAVE BROKEN SAMPLING!\n";
      Rcout << (twoScores[0] + twoScores[1]) << " x " << this->probabilityMaxOptim << "\n";
    }
    
    
    for(uint32_t i = 0; i < this->input_data.N; i++)
    {
      this->current_id = i;
      
      uint32_t flatindex = riter[i] - 1;
      uint32_t lindex = 0;
      uint32_t xyindex = flatindex;
      
      uint32_t sumSizes = 0;
      while(true)
      {
        uint32_t sizeMatrix = (*this->input_data.sizes)[lindex] * (*this->input_data.sizes)[lindex + 1];
        sumSizes += sizeMatrix;
        
        if(sumSizes > flatindex)
        {
          break;
        }
        lindex++;
        xyindex -= sizeMatrix;
      }
      
      const IntegerMatrix& sk = (*SK_list)[lindex];
      uint32_t r = xyindex % sk.nrow();
      uint32_t c = xyindex / sk.nrow();
      
      if(i % PRINT_FREQ == 0)
      {
        this->DebugInfo(SKSS_PHASE, i);
      }
      
      if(i % SAVE_FREQ == 0)
      {
        this->SaveGraph();
      }
      
      // if inside G_0 -> skip
      if((*this->G[lindex])(r,c) != 0)      
        continue;
      
      // try add an edge outside of G_0
      // do not update V_G since we want to sample from partialSK-SS only!
      // because of that -> shouldUpdateTrie = false
      this->tryPerformMove(&twoScores, lindex, r, c, ChangeType::ADD, false);
      
    }
  }
  
  
  void partialSK_SS_iter()
  {
    NumericVector twoScores = this->callScoreFull();
    const List* SK_list = this->input_data.SK;
    
    IntegerVector riter = Rcpp::sample(
      this->input_data.N, this->input_data.N);
    //saveRDS(riter, Rcpp::Named("file", "sampled_indices.RData"));
    Rprintf("partialSKSS-add-only phase\n");
    
    for(uint32_t i = 0; i < this->input_data.N; i++)
    {
      this->current_id = i;
      
      uint32_t flatindex = riter[i] - 1;
      uint32_t lindex = 0;
      uint32_t xyindex = flatindex;
      
      uint32_t sumSizes = 0;
      while(true)
      {
        uint32_t sizeMatrix = (*this->input_data.sizes)[lindex] * (*this->input_data.sizes)[lindex + 1];
        
        sumSizes += sizeMatrix;
        
        if(sumSizes > flatindex)
        {
          break;
        }
        lindex++;
        xyindex -= sizeMatrix;
      }
      
      const IntegerMatrix& sk = (*SK_list)[lindex];
      uint32_t r = xyindex % sk.nrow();
      uint32_t c = xyindex / sk.nrow();
      
      if(i % (PRINT_FREQ * 1000) == 0)
      {
        this->DebugInfo(PARTIAL_ADD_ONLY, i);
      }
      if(sk(r, c) == 0) 
      {
        continue;
      }
      this->tryPerformMove(&twoScores, lindex, r, c, ChangeType::ADD);
      
    }
    
    Rprintf("partialSKSS-add-remove phase\n");
    riter = Rcpp::sample(
      this->input_data.N, this->input_data.N);
    
    for(uint32_t i = 0; i < this->input_data.N; i++)
    {
      uint32_t flatindex = riter[i] - 1;
      uint32_t lindex = 0;
      uint32_t xyindex = flatindex;
      
      uint32_t sumSizes = 0;
      while(true)
      {
        uint32_t sizeMatrix = (*this->input_data.sizes)[lindex] * (*this->input_data.sizes)[lindex + 1];
        
        sumSizes += sizeMatrix;
        
        if(sumSizes > flatindex)
        {
          break;
        }
        lindex++;
        xyindex -= sizeMatrix;
      }
      
      const IntegerMatrix& sk = (*SK_list)[lindex];
      uint32_t r = xyindex % sk.nrow();
      uint32_t c = xyindex / sk.nrow();
      
      if(i % PRINT_FREQ == 0)
      {
        this->DebugInfo(PARTIAL_ADD_REMOVE, i);
      }
      //if(i % SAVE_FREQ == 0)
      //  this->SaveGraph();
      
      if(sk(r, c) == 0) 
      {
        continue;
      }
      
      LogicalMatrix& gz = *(this->G[lindex]);
      
      if(gz(r,c) == 0)  // if not in G -> try add
        this->tryPerformMove(&twoScores, lindex, r, c, ChangeType::ADD); 
      else              // if in G -> try delete
        this->tryPerformMove(&twoScores, lindex, r, c, ChangeType::DELETE); 
    }
  }
  
  void DebugInfo(std::string phase, uint32_t iter)
  {
    Rprintf("%s[%5u]/[%5u][Trie:%5u|%5u][Score:%9.6f]\n", 
            phase.c_str(), iter, this->input_data.N, 
            this->V_G.num_graphs(), this->V_G.num_nodes(),
            this->probabilityMaxOptim);
  }
  
  List& G_to_list()
  {
    const List* SK_list = this->input_data.SK;
    List* ret = new List(SK_list->length());
    
    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      (*ret)[i] = *this->G[i];
    }
    
    return *ret;
  }
  
  List& G_optim_to_list()
  {
    const List* SK_list = this->input_data.SK;
    List* ret = new List(SK_list->length());
    
    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      (*ret)[i] = *this->G_optim[i];
    }
    
    return *ret;
  }
  
  void resetSampledG()
  {
    const List* SK_list = this->input_data.SK;
    
    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      const IntegerMatrix& sk = (*SK_list)[i];
      
      *(this->G_sampled)[i] = LogicalMatrix(sk.nrow(), sk.ncol());
      Rcpp::rownames(*(this->G_sampled)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G_sampled)[i]) = Rcpp::colnames(sk);
    }
  }
  
  void emptyCurrCountsAndG()
  {
    const List* SK_list = this->input_data.SK;
    
    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      const IntegerMatrix& sk = (*SK_list)[i];
      
      //*(this->G)[i] = LogicalMatrix(sk.nrow(), sk.ncol());
      *(this->G)[i] = *(this->G_sampled)[i]; 
      Rcpp::rownames(*(this->G)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G)[i]) = Rcpp::colnames(sk);
    }
  }
  
  /**
   * We should convert Rcpp lists to vectors of matrices because:
   *    > we can't easily manipulate Rcpp List objects
   *    > with matrix pointers it is easier to manage memory manually
   *    > BUT! we should still use matrices to call R score functions
   */
  void initializeLists()
  {
    const List* SK_list = this->input_data.SK;
    
    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      const IntegerMatrix& sk = (*SK_list)[i];
      
      (this->G).push_back(new LogicalMatrix(sk.nrow(), sk.ncol()));
      Rcpp::rownames(*(this->G)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G)[i]) = Rcpp::colnames(sk);
      
      (this->G_optim).push_back(new LogicalMatrix(sk.nrow(), sk.ncol()));
      Rcpp::rownames(*(this->G_optim)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G_optim)[i]) = Rcpp::colnames(sk);
      
      (this->G_sampled).push_back(new LogicalMatrix(sk.nrow(), sk.ncol()));
      Rcpp::rownames(*(this->G_sampled)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G_sampled)[i]) = Rcpp::colnames(sk);
    }
    
  }
  
  /**
   * Clear matrices in vectors
   */
  void deleteLists()
  {
    const List* SK_list = this->input_data.SK;
    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      delete this->G[i];
      delete this->G_optim[i];
    }
    
    this->G.clear();
    this->G_optim.clear();
  }
  

  void tryPerformMove(NumericVector* score, uint32_t listID, uint32_t from, uint32_t to, ChangeType action, 
                      bool shouldUpdateTrie = true)
  {
    // Compute the possible score
    NumericVector scoreChanges = this->callScoreChange(score, listID, from, to, action);
    
    /**
     * Modified(explicit per-partes) is used, e.g.:
     * 
     * since we are working in small precisions + log scale:
     *   > difference is calculated in SCORE functions manually
     *   > difference is separately calculated for BGe / Energy part
     *   > the previous is because they have orders of magnitude differences
     *   > ONLY afterwards the summed difference is converted by exp, since it is tractable!
     */
    double mod_diff = (scoreChanges[3] + scoreChanges[4]);
    double diff = exp(mod_diff);
    
    // Convert the log difference to B/A and normalize it
    // normalize(1, B/A) = normalize(A,B)
    // that way we get probabilities for two candidates!
    NumericVector probs = {1.0, diff};
    probs = probs / Rcpp::sum(probs);
    IntegerVector ids = {0, 1};
    IntegerVector ret = Rcpp::sample(ids, 1, probs = probs);
    
    // Save visited score
    this->visitedScores.push_back(scoreChanges[0] + scoreChanges[1]);
    
    // If the first was sampled, skip
    if(ret[0] != 1) return;
    
    // otherwise update current state:

    // Update score
    *score = scoreChanges;
    
    // Update G if not in SK-SS part
    this->makeMove(listID, from, to, action, shouldUpdateTrie);
    
    // Update optimum
    this->CheckVisitedGraphOptimum(scoreChanges);
  }
  
  
  
  void makeMove(uint32_t listID, uint32_t from, uint32_t to, ChangeType action, bool shouldUpdateTrie)
  {
    // update adjacency
    (*(this->G[listID]))(from,to) = action == ChangeType::ADD;
    
    // Update our ordered add-only word
    key_tp kt = std::make_tuple((uint8_t)listID, (uint16_t)from, (uint16_t)to);
    if(action == ChangeType::ADD)
    {
      insert_sorted(this->ord_V_G, kt);
    }
    else{
      erase_sorted(this->ord_V_G, kt);
    }
    
    // If SK-SS part -> don't update Trie
    if(!shouldUpdateTrie)
    {
      return;
    }
    
    // Try to add word to Trie
    this->V_G.AppendChain(this->ord_V_G);
    
    key_action_tp to_push;
    to_push.type = action;
    to_push.coord = kt;
    //this->unord_V_G.push_back(to_push);
  }
  
  void SaveGraph()
  {
    // Save intermediate results
    Rprintf("Saving intermediate results [%9.6f]\n", this->probabilityMaxOptim);
    
    // Save optimum
    List& tmp_adjs = this->G_optim_to_list();
    char fileName[50];
    char fileName2[50];
    sprintf(fileName, "optimG_%9.6f.RData", this->probabilityMaxOptim);
    sprintf(fileName2, "optimG_%9.6f_copy.RData", this->probabilityMaxOptim);
    saveRDS(tmp_adjs, Rcpp::Named("file", fileName));
    saveRDS(tmp_adjs, Rcpp::Named("file", fileName2));
    
    // Save current
    tmp_adjs = this->G_to_list();
    List ret = List(2);
    ret[0] = this->current_id;
    ret[1] = tmp_adjs;
    saveRDS(ret, Rcpp::Named("file", "G_last.RData"));
    
    // Save current
    /*
    char fileName3[50];
    tmp_adjs = this->G_to_list();
    sprintf(fileName3, "G_%d.RData", this->current_id);
    saveRDS(tmp_adjs, Rcpp::Named("file", fileName3));*/

    // Save visited scores and reset them
    saveRDS(visitedScores, Rcpp::Named("file", "visited_scores.RData"));
    saveRDS(visitedScores, Rcpp::Named("file", "visited_scores_copy.RData"));
    
    saveRDS(acceptedScores, Rcpp::Named("file", "accepted_scores.RData"));
    saveRDS(acceptedScores, Rcpp::Named("file", "accepted_scores_copy.RData"));
  }
  
  void CheckVisitedGraphOptimum(NumericVector& scoreChanges)
  {
    double bge = scoreChanges[1];
    double energy = scoreChanges[0];
    double probabilityMovedG = bge + energy;
    
    this->acceptedScores.push_back(probabilityMovedG);
    
    if(probabilityMovedG <= this->probabilityMaxOptim)
    {
      return;
    }
    
    //Rcout << "New optim found! " << this->probabilityMaxOptim << "x" << probabilityMovedG << "\n";
    //Rcout << sum(NumericMatrix(*this->G_optim[0])) << " x " << sum(NumericMatrix(*this->G[0])) << "\n";
    this->probabilityMaxOptim = probabilityMovedG;
    
    const List* SK_list = this->input_data.SK;
    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      *this->G_optim[i] = Rcpp::LogicalMatrix(
        Rcpp::clone(*this->G[i])
      );
    }

  }
  
  
  
  NumericVector callScoreChange(NumericVector* score, uint32_t listID, uint32_t from, uint32_t to, ChangeType action)
  {
    List& adjs = this->G_to_list();
    NumericVector scoreChanges = (*this->input_data.score_change_func)
      (
          adjs, 
          *this->input_data.PK, 
          this->settings.beta_L, 
          this->settings.beta_H, 
          *this->input_data.bge_param,
          *this->input_data.sizes,
          *score, 
          listID,
          from, 
          to, 
          action == ChangeType::ADD);
    
    delete &adjs;
    return scoreChanges;
  }
  
  NumericVector callScoreFull()
  {
    List& adjs = this->G_to_list();
    NumericVector twoScores = (*this->input_data.score_full_func)(
      adjs, 
      *this->input_data.PK,
      this->settings.beta_L,
      this->settings.beta_H,
      *this->input_data.bge_param
    );
    delete &adjs;  
    return twoScores;
  }
};

/**
 * This function is the interface to the R part, the only function exported
 */
List c_SK_SS(
                         const List& PK,
                         const List& SK,
                         const IntegerVector& sizes,
                         const List& param,
                         uint32_t I,
                         bool debug,
                         double beta_L, double beta_H,
                         Function score_full_func,
                         Function score_change_func);


/**
 * This part is the implementation of above header declarations, it goes to .CPP part in package
 */
//#include "skeleton.h"

// [[Rcpp::export]]
List c_SK_SS(
                  const List& PK,
                  const List& SK,
                  const IntegerVector& sizes,
                  const List& param,
                  uint32_t I,
                  bool debug,
                  double beta_L, double beta_H,
                  double cutoff_ratio,
                  Function score_full_func,
                  Function score_change_func
                  )
{
  uint32_t N = 0;
  for(uint32_t i = 0; i < (sizes.size() - 1); i++)
  {
    N += sizes[i] * sizes[i+1];
  }
  Rprintf("N = %u\n", N);
  
  IData i_data;
  
  i_data.PK = &PK;
  i_data.SK = &SK;
  i_data.sizes = &sizes;
  i_data.bge_param = &param;
  i_data.N = N;
  
  i_data.score_full_func = &score_full_func;
  i_data.score_change_func = &score_change_func;
  
  ISettings settings;
  
  settings.I = I;
  settings.debug = debug;
  settings.beta_H = beta_H;
  settings.beta_L = beta_L;
  settings.cutoff_ratio = cutoff_ratio;

  Network* net = new Network(i_data, settings);
  
  net->SK_SS();
  List ret = net->result();
  delete net;
  
  return ret;
}
