BGe_param_init <- function(...)
{
  additional_params <- list(...)
  GE <- additional_params$GE
  dist <- additional_params$dist
  
  # prepare BGe score parameters
  # this is a large computation that computes a large parameter matrix:
  # N x N, where N is the total number of genes
  myScore <- scoreparameters_BiDAG_BGe(n = ncol(GE), data = GE)
  
  param_in <- list(
    score_func = BGE_score,
    change_func = BGe_change,
    n=myScore$n, awpN=myScore$awpN, TN=myScore$TN, scoreconstvec=myScore$scoreconstvec, prevscores=list())

  return(param_in)
}

BGe_change <- function(move, G_adjs, param)
{
  # Extract input values
  part_id <- move$listID
  from <- move$from
  to <- move$to
  action_is_add <- move$action_is_add
  n <- param$n
  score_node_prev <- param$prevscores[[part_id]][to]
  adj <- G_adjs[[part_id]]
  node <- colnames(adj)[to]
  
  
  # Change G_adjs, compute new score for a node
  adj[from, to] <- action_is_add
  neights <- names(which(adj[,node] > 0))
  score_node <- BGe_node_compute(node, neights, param, n)
  adj[from, to] <- (1 - action_is_add)
  
  # Compute a log change in a score
  param$diff <- score_node - score_node_prev
  param$score <- param$score + param$diff
  
  # Update params and return
  param$prevscores[[part_id]][to] <- score_node
  
  return(param)
}

BGE_score <- function(G_adjs, param)
{
  pDG1 <- 0
  # store every node's computed BGe subscore
  param$prevscores <- list()
  
  for(i in 1:length(G_adjs)) 
  {
    adj <- G_adjs[[i]]
    param$prevscores[[i]] <- BGe_compute(adj, param)
    pDG1 <- pDG1 + sum(param$prevscores[[i]])
  }

  param$score <- pDG1
  param$diff <- 0
  
  return(param)
}




#'
#' BGe score computation, taken from IntOMICS/BiDAG implementation:
#' <DAGcorescore_BiDAG_BGe.R>
#'
BGe_compute <- function(adj, param)
{
  nodes_cand <- colnames(adj)
  sc <- sapply(nodes_cand, function(node)
  {
    neights <- names(which(adj[,node] > 0))
    n <- param$n
    score <- BGe_node_compute(node, neights, param, n)
    return(score)
  })
  names(sc) <- nodes_cand
  return(sc)
}


BGe_node_compute <- function(j,parentnodes, param, n)
{
  #n <- param$n
  TN <- param$TN
  awpN <- param$awpN
  scoreconstvec <- param$scoreconstvec
  
  lp <- length(parentnodes) #number of parents
  awpNd2 <- (awpN-n+lp+1)/2
  A <- TN[j,j]
  
  switch(as.character(lp),
         "0"={# just a single term if no parents
           corescore <- scoreconstvec[lp+1] -awpNd2*log(A)
         },
         
         "1" = {# no need for matrices
           D <- TN[parentnodes,parentnodes]
           logdetD <- log(D)
           B <- TN[j,parentnodes]
           logdetpart2 <- log(A-B^2/D)
           corescore <- scoreconstvec[lp+1]-awpNd2*logdetpart2 - logdetD/2
         },
         
         "2" = {
           D <- TN[parentnodes,parentnodes]
           
           dettwobytwo <- function(D) {
             D[1,1]*D[2,2]-D[1,2]*D[2,1]
           }
           
           detD <- dettwobytwo(D)
           logdetD <- log(detD)
           B <- TN[j,parentnodes]
           logdetpart2 <- log(dettwobytwo(D-(B)%*%t(B)/A))+log(A)-logdetD
           corescore <- scoreconstvec[lp+1]-awpNd2*logdetpart2 - logdetD/2
         },
         
         {# otherwise we use cholesky decomposition to perform both
           D<-as.matrix(TN[parentnodes,parentnodes])
           choltemp<-chol(D)
           logdetD<-2*log(prod(choltemp[(lp+1)*c(0:(lp-1))+1]))
           B<-TN[j,parentnodes]
           logdetpart2<-log(A-sum(backsolve(choltemp,B,transpose=TRUE)^2))
           corescore <- scoreconstvec[lp+1]-awpNd2*logdetpart2 - logdetD/2
         })
  
  return(corescore)
}

####################################################################################################
## This function is from BiDAG package (https://cran.r-project.org/web/packages/BiDAG/index.html) ##
## parameters needed for calculation of the BGe score                                              ##
## see arXiv:1402.6863                                                                            ##
####################################################################################################
scoreparameters_BiDAG_BGe <- function (n, 
                                       data, 
                                       bgepar = list(am = 1, aw = NULL))
{

  if (anyNA(data)) {
    print("Warning: Dataset contains missing data (covariance matrix computation: complete.obs parameter - missing values are handled by casewise deletion)")
  }

  if (all(is.character(colnames(data)))) {
    nodeslabels <- colnames(data)
  } else {
    nodeslabels <- sapply(c(1:n), function(x) paste("v", x, sep = ""))
  }

  colnames(data) <- nodeslabels
  initparam <- list()
  initparam$labels <- nodeslabels
  initparam$type <- "bge"
  initparam$DBN <- FALSE
  initparam$weightvector <- NULL
  initparam$data <- data

  initparam$bgnodes <- NULL
  initparam$static <- NULL
  initparam$mainnodes <- c(1:n)
  
  initparam$bgn <- 0
  initparam$n <- n
  initparam$nsmall <- n
  initparam$labels.short <- initparam$labels
  initparam$logedgepmat <- NULL

  N <- nrow(data)
  if(N==1)
  {
    covmat <- matrix(0,nrow=n, ncol=n, dimnames=list(initparam$labels.short,initparam$labels.short))
  } else {
    covmat <- cov(data, use = "complete.obs") * (N - 1)
  } # end if else (N==1)
  means <- colMeans(data, na.rm = TRUE)
  bgepar$aw <- n + bgepar$am + 1
  
  initparam$am <- bgepar$am
  initparam$aw <- bgepar$aw
  initparam$N <- N
  initparam$means <- means
  mu0 <- numeric(n)
  T0scale <- bgepar$am * (bgepar$aw - n - 1)/(bgepar$am + 1)
  T0 <- diag(T0scale, n, n)
  initparam$TN <- T0 + covmat + ((bgepar$am * N)/(bgepar$am + N)) * (mu0 - means) %*% t(mu0 - means)
  initparam$awpN <- bgepar$aw + N
  constscorefact <- -(N/2) * log(pi) + (1/2) * log(bgepar$am/(bgepar$am +  N))
  initparam$muN <- (N * means + bgepar$am * mu0)/(N + bgepar$am)
  initparam$SigmaN <- initparam$TN/(initparam$awpN - n - 1)
  initparam$scoreconstvec <- numeric(n)
  for (j in (1:n)) {
    awp <- bgepar$aw - n + j
    initparam$scoreconstvec[j] <- constscorefact - lgamma(awp/2) + 
      lgamma((awp + N)/2) + ((awp + j - 1)/2) * log(T0scale)
  }
  attr(initparam, "class") <- "scoreparameters"
  initparam
}
