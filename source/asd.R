source("source/testing.R")
source("source/skeleton.R")
library(bnlearn)

tst <- function()
{
  
  
  results <- data.frame(matrix(NA, 0, 4))
  colnames(results) <- c("Skeleton", "MAP", "Posterior", "SK-SS")
  for(i in 1:10)
  {
    data <- generate_data(N_samples = 5, prior_FP=0.0, prior_FN=0.5, p_rand = 0.5,
                          n_circ = 3, n_mirna = 2, n_mrna = 6)
    data$SigmoidData <- 10^6 / (1 + exp(-data$Data))
    parts <- data$partition
    PK <- data$PK_adjacency
    
    real <- factor(
      c(data$Real_adjacency[parts[[1]], parts[[2]]], data$Real_adjacency[parts[[2]], parts[[3]]]), 
      levels = c(0, 1))
    
    # Skeleton local method
    SK <- iamb.fdr(x=data$SigmoidData, undirected = TRUE)
    SK <- bnlearn::amat(SK)
    SK <- data.frame(SK)
    pred <- factor(
      unlist(c(SK[parts[[1]], parts[[2]]], SK[parts[[2]], parts[[3]]])), 
      levels = c(0, 1))
    confusion_mat <- caret::confusionMatrix(pred,real)
    shd_sk <- confusion_mat$table[1,2] + confusion_mat$table[2,1]
    
    ret <- test_all_structures(data)
    ret_post <- ret$posterior
    ret_post <- lapply(ret_post, function(m) {
      storage.mode(m) <- "numeric"
      m
    })
    ret_map <- ret$map
    ret_map <- lapply(ret_map, function(m) {
      storage.mode(m) <- "numeric"
      m
    })
    
    # Posterior
    pred_post <- factor(
      unlist(ret_post), 
      levels = c(0, 1))
    confusion_mat <- caret::confusionMatrix(pred_post,real)
    shd_post <- confusion_mat$table[1,2] + confusion_mat$table[2,1]
    # MAP
    pred_map <- factor(
      unlist(ret_map), 
      levels = c(0, 1))
    confusion_mat <- caret::confusionMatrix(pred_map,real)
    shd_map <- confusion_mat$table[1,2] + confusion_mat$table[2,1]
    
    # SK-SS
    ret <- SK_SS_tripartite(data$SigmoidData,data$PK_adjacency,SK,parts=data$partition, I=20)
    PK_out <- ret$PK_out
    storage.mode(PK_out) <- "numeric"
    pred_skss <- factor(
      unlist(c(PK_out[parts[[1]], parts[[2]]], PK_out[parts[[2]], parts[[3]]])), 
      levels = c(0, 1))
    confusion_mat <- caret::confusionMatrix(pred_skss,real)
    shd_skss <- confusion_mat$table[1,2] + confusion_mat$table[2,1]
    if(shd_skss == 0)
    {
      ad <- 1
    }
    
    
    cat("Iter:", i, ", SK:", shd_sk, ", MAP:", shd_map, ", posterior: ", shd_post, "skss:", shd_skss, "\n")
    results <- rbind(results, c(shd_sk, shd_map, shd_post, shd_skss))
  }
  results <<- results
}