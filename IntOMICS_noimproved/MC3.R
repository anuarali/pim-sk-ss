#' @export
MC3 <- function(source_net, omics, layers_def, B_prior_mat, beta.source, partition_func_UB_beta_source, parent_set_combinations, BGe_score_all_configs_node, annot)
{
  ge_nodes <- rownames(source_net$adjacency)[regexpr("ENTREZ", rownames(source_net$adjacency))>0]
  vec <- 1:length(c(source_net$adjacency))
  vec <- vec[c(B_prior_mat)>0]
  edge_proposal_res <- edge_proposal(net = source_net$adjacency, candidates = vec, layers_def = layers_def, ge_nodes = ge_nodes, omics = omics, B_prior_mat = B_prior_mat)
  
  while(edge_proposal_res$no_action | !is.acyclic(edge_proposal_res$net))
  {
    vec <- vec[vec!=edge_proposal_res$edge]
    edge_proposal_res <- edge_proposal(net = source_net$adjacency, candidates = vec, layers_def = layers_def, ge_nodes = ge_nodes, omics = omics, B_prior_mat = B_prior_mat)
  } # end while(edge_proposal_res$no_action | !is.acyclic(edge_proposal_res$net))

  if(edge_proposal_res$edge_move=="add")
  {
    parents_source_target <- names(which(source_net$adjacency[,edge_proposal_res$col]==1))
    lp <- length(parents_source_target)
    if(lp>0)
    {
      ind_BGe_source <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp]], 2, FUN=function(col) all(parents_source_target %in% col))
      BGe_node_source <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp]][ind_BGe_source]
      ind_BGe_candidate <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp+1]], 2, FUN=function(col) length(intersect(c(parents_source_target,colnames(source_net$adjacency)[edge_proposal_res$row]),col))>lp)
      BGe_node_candidate <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp+1]][ind_BGe_candidate]
    } else {
      ind_BGe_source <- is.na(c(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]]))
      BGe_node_source <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]][ind_BGe_source]
      ind_BGe_candidate <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]], 2, FUN=function(col) (colnames(source_net$adjacency)[edge_proposal_res$row] %in% col))
      BGe_node_candidate <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]][ind_BGe_candidate]
    }# end if else (lp>0)
    ### Marginal likelihood P(D|G) using BGe score:
    BGe <- source_net$BGe - BGe_node_source + BGe_node_candidate
  } else if (edge_proposal_res$edge_move=="delete")
  {
    parents_source_target <- names(which(source_net$adjacency[,edge_proposal_res$col]==1))
    lp <- length(parents_source_target)
    if(lp>1)
    {
      ind_BGe_source <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp]], 2, FUN=function(col) all(parents_source_target %in% col))
      BGe_node_source <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp]][ind_BGe_source]
      ind_BGe_candidate <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp-1]], 2, FUN=function(col) length(intersect(setdiff(parents_source_target,colnames(source_net$adjacency)[edge_proposal_res$row]),col))==(lp-1))
      BGe_node_candidate <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp-1]][ind_BGe_candidate]
    } else {
      ind_BGe_source <- which(c(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]])==parents_source_target)
      BGe_node_source <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]][ind_BGe_source]
      ind_BGe_candidate <- is.na(c(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]]))
      BGe_node_candidate <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]][ind_BGe_candidate]
    } # end if else (lp>0)
    ### Marginal likelihood P(D|G) using BGe score:
    BGe <- source_net$BGe - BGe_node_source + BGe_node_candidate
  } else {
    # delete
    parents_source_target <- names(which(source_net$adjacency[,edge_proposal_res$col]==1))
    lp <- length(parents_source_target)
    if(lp>1)
    {
      ind_BGe_source <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp]], 2, FUN=function(col) all(parents_source_target %in% col))
      BGe_node_source <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp]][ind_BGe_source]
      ind_BGe_candidate <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp-1]], 2, FUN=function(col) length(intersect(setdiff(parents_source_target,colnames(source_net$adjacency)[edge_proposal_res$row]),col))==(lp-1))
      BGe_node_candidate <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[lp-1]][ind_BGe_candidate]
    } else {
      ind_BGe_source <- which(c(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]])==parents_source_target)
      BGe_node_source <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]][ind_BGe_source]
      ind_BGe_candidate <- is.na(c(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]]))
      BGe_node_candidate <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$col]]][[1]][ind_BGe_candidate]
    } # end if else (lp>0)
    ### Marginal likelihood P(D|G) using BGe score:
    BGe <- source_net$BGe - BGe_node_source + BGe_node_candidate
    # add
    parents_candidate_target <- names(which(source_net$adjacency[,edge_proposal_res$row]==1))
    lp <- length(parents_candidate_target)
    if(lp>0)
    {
      ind_BGe_source <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$row]]][[lp]], 2, FUN=function(col) all(parents_candidate_target %in% col))
      BGe_node_source <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$row]]][[lp]][ind_BGe_source]
      ind_BGe_candidate <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$row]]][[lp+1]], 2, FUN=function(col) length(intersect(c(parents_candidate_target,colnames(source_net$adjacency)[edge_proposal_res$col]),col))>lp)
      BGe_node_candidate <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$row]]][[lp+1]][ind_BGe_candidate]
    } else {
      ind_BGe_source <- is.na(c(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$row]]][[1]]))
      BGe_node_source <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$row]]][[1]][ind_BGe_source]
      ind_BGe_candidate <- apply(parent_set_combinations[[colnames(source_net$adjacency)[edge_proposal_res$row]]][[1]], 2, FUN=function(col) length(intersect(colnames(source_net$adjacency)[edge_proposal_res$col],col))==1)
      BGe_node_candidate <- BGe_score_all_configs_node[[colnames(source_net$adjacency)[edge_proposal_res$row]]][[1]][ind_BGe_candidate]
    }# end if else (lp>0)
    ### Marginal likelihood P(D|G) using BGe score:
    BGe <- BGe - BGe_node_source + BGe_node_candidate
  } # end if(edge_proposal_res$edge_move=="add") else if (edge_proposal_res$edge_move=="delete")
  
  # source_net_adjacency <- edge_proposal_res$net
  nbhd.size <- neighborhood_size(net = edge_proposal_res$net, layers_def = layers_def, B_prior_mat = B_prior_mat, omics = omics)
  
  ## Prior probability P(G)
  energy <- sum(epsilon(net = edge_proposal_res$net, B_prior_mat = B_prior_mat))
  prior <- (-beta.source$value*energy) - partition_func_UB_beta_source
  
  likelihood_part <- BGe + prior
  # edge_move <- edge_proposal_res$edge_move
  
  return(list(adjacency = edge_proposal_res$net, nbhd.size = nbhd.size, proposal.distr = c(), energy = energy, prior = prior, BGe = BGe, likelihood_part = likelihood_part, likelihood = c(), acceptance = c(), edge_move = edge_proposal_res$edge_move))
}
