#' @export
source_net_def <- function(init.net.mcmc.output, omics, parent_set_combinations, BGe_score_all_configs_node, B_prior_mat, layers_def, energy_all_configs_node, len, imprvd_data)
{
  beta_init <- runif(1, min = 0, max = 10)
  beta.source <- list(value = beta_init, prior = c())
  source.net <- init.net.mcmc.output$source.net
  ### source.net parameters necessary for MC3 method:
  ## Marginal likelihood P(D|G) using ln(BGe score):
  source.net$BGe <- BGe_score(adjacency_matrix = source.net$adjacency, omics = omics, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node)
  source.net$nbhd.size <- neighborhood_size(net = source.net$adjacency, layers_def = layers_def, B_prior_mat = B_prior_mat, omics = omics)
  ## Prior probability
  source.net$energy <- sum(epsilon(net = source.net$adjacency, B_prior_mat = B_prior_mat))
  partition_func_UB_beta_source <- sum(mapply(energy_all_configs_node,FUN=function(x) logSumExp(-beta.source$value*x)))
  # partition_func_UB is already log transformed
  # the output of this function is also log transformed (natural logarithm)!
  source.net$prior <- (-beta.source$value*source.net$energy) - partition_func_UB_beta_source
  source.net$likelihood_part <- source.net$BGe + source.net$prior

  beta.source$prior <- source.net$prior
  beta.source$len <- len
  acceptance_saved <- vector("numeric")
  acceptance_beta_saved <- vector("numeric")
  method_choice_saved <- vector("numeric")
  nets <- list()
  nets[[1]] <- source.net
  betas <- list()
  betas[[1]] <- beta.source
  return(list(source.net=source.net,
              partition_func_UB_beta_source=partition_func_UB_beta_source,
              beta.source = beta.source,
              acceptance_saved = acceptance_saved,
              acceptance_beta_saved = acceptance_beta_saved,
              method_choice_saved = method_choice_saved,
              energy_all_configs_node = energy_all_configs_node,
              nets = nets,
              B_prior_mat = B_prior_mat,
              betas = betas))
}
