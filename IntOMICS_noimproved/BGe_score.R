#' ##########################################################################################
#' ## Compute the BGe score of given network using precomputed sets of possible GE parents ##
#' ##########################################################################################
#' @export
BGe_score <- function(adjacency_matrix, omics, layers_def, parent_set_combinations, BGe_score_all_configs_node)
{
  nodes_cand <- rownames(adjacency_matrix)[which(regexpr("ENTREZ",rownames(adjacency_matrix))>0)]
  score_nodes <- sum(sapply(nodes_cand, FUN=function(node) BGe_node(node = node, adjacency_matrix = adjacency_matrix, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node)))
  return(score_nodes)
}