#' # this function define random network from given empty network
#' # this function is from bnstruct R package
#' # sample a random chain from features of an empty network (modified to create edges only between GE!)
#' @export
sample.chain <- function(empty_net, omics_ge, seed)
{
  dataset_BND <- BNDataset(data = empty_net,
                           discreteness = rep('d',ncol(empty_net)),
                           variables = c(colnames(empty_net)),
                           node.sizes = rep(2,ncol(empty_net)), starts.from=0)
  # The number of values a variable can take is called its cardinality
  # node.sizes: vector of variable cardinalities (for discrete variables)
  # discretenes: now I use 'd', even if the values are continuous
  
  net <- BN(dataset_BND)
  net.dag <- dag(net)
  if(missing(seed))
  {
    n <- ncol(omics_ge)
    chain <- sample(n,n)
    for( i in 2:n )
    {
      net.dag[chain[i-1],chain[i]] <- 1
    }
    dag(net) <- net.dag
    return( suppressMessages(learn.params(net,dataset_BND)) )
  } else {
    set.seed(seed)
    n <- ncol(omics_ge)
    chain <- sample(n,n)
    for( i in 2:n )
    {
      net.dag[chain[i-1],chain[i]] <- 1
    }
    dag(net) <- net.dag
    return( suppressMessages(learn.params(net,dataset_BND)) )
  }
  
}
