#' ##########################
#' ### 2nd adaption phase ###
#' ### modified Adaptive Metropolis algorithm to find the proposal distribution that has a similar covariance structure with the target distribution
#' @export
second_adapt_phase <- function(transient.phase_net, omics, layers_def, B_prior_mat, energy_all_configs_node, prob_mbr, BGe_score_all_configs_node, parent_set_combinations, annot, woPKGE_belief = 0.5) 
{
  # constant <- 2.38/1.5
  # for details see the original paper
  second.adapt.phase_net <- variance_target(transient.phase_net = transient.phase_net, 
                                            constant = 1.586667, 
                                            fin = 200, 
                                            B_prior_mat = B_prior_mat, 
                                            omics = omics, 
                                            parent_set_combinations = parent_set_combinations, 
                                            BGe_score_all_configs_node = BGe_score_all_configs_node, 
                                            layers_def = layers_def,
                                            prob_mbr = prob_mbr,
                                            annot = annot)
  i <- 1
  while(second.adapt.phase_net$acceptance.rate_betas < 0.02)
  {
    i <- i+1
    constant <- 2.38/(1.5^i)
    second.adapt.phase_net <- variance_target(transient.phase_net = transient.phase_net, 
                                              constant = constant, 
                                              fin = 200, 
                                              B_prior_mat = B_prior_mat, 
                                              omics = omics, 
                                              parent_set_combinations = parent_set_combinations, 
                                              BGe_score_all_configs_node = BGe_score_all_configs_node, 
                                              layers_def = layers_def,
                                              prob_mbr = prob_mbr,
                                              annot = annot)
    
  } # end while(second.adapt.phase_net$acceptance.rate_betas < 0.02)

  # Now calculate the average squared jumping distance for beta (beta_n - beta_n-1)^2.
  # With 5 different averages we fit a linear model to see if there is any trend in squared jumping distance for each coordinate.
  # If the average squared jumping distance stops to increase, the mixing stops to improve and we stop this phase.
  constant <- 2.38/(1.5^i)
  squared.jump_second.adapt.phase_net <- squared_jumping(second.adapt.phase_net = second.adapt.phase_net$variance.target_net, 
                                                         constant = constant,
                                                         beta_sd = second.adapt.phase_net$beta_sd,
                                                         fin = (nrow(B_prior_mat)^2)*5, 
                                                         B_prior_mat = B_prior_mat, 
                                                         omics = omics, 
                                                         parent_set_combinations = parent_set_combinations, 
                                                         BGe_score_all_configs_node = BGe_score_all_configs_node, 
                                                         layers_def = layers_def,
                                                         prob_mbr = prob_mbr,
                                                         annot = annot)

  betas_check <- mapply(tail(squared.jump_second.adapt.phase_net$betas,1001), FUN=function(list) list$value)
  if(length(unique(betas_check))>1)
  {
    betas_check <- colMeans(matrix((betas_check[-1] - betas_check[-1001])^2,nrow=200))
    reg_dat <- data.frame(beta_means = betas_check, iter = 1:5)
    model <- lm(beta_means ~ iter, data = reg_dat)
    squared.jump_second.adapt.phase_net$p.val <- summary(model)$coefficients[2,4]
    
    while(squared.jump_second.adapt.phase_net$p.val < 0.1)
    {
      squared.jump_second.adapt.phase_net <- squared_jumping(second.adapt.phase_net = squared.jump_second.adapt.phase_net, 
                                                             constant = constant,
                                                             beta_sd = second.adapt.phase_net$beta_sd,
                                                             fin = 200, 
                                                             B_prior_mat = B_prior_mat, 
                                                             omics = omics, 
                                                             parent_set_combinations = parent_set_combinations, 
                                                             BGe_score_all_configs_node = BGe_score_all_configs_node, 
                                                             layers_def = layers_def,
                                                             prob_mbr = prob_mbr,
                                                             annot = annot)
      
      betas_check <- mapply(tail(squared.jump_second.adapt.phase_net$betas,1001), FUN=function(list) list$value)
      if(length(unique(betas_check))>1)
      {
        betas_check <- colMeans(matrix((betas_check[-1] - betas_check[-1001])^2,nrow=200))
        reg_dat <- data.frame(beta_means = betas_check, iter = 1:5)
        model <- lm(beta_means ~ iter, data = reg_dat)
        squared.jump_second.adapt.phase_net$p.val <- summary(model)$coefficients[2,4]
      } else {
        squared.jump_second.adapt.phase_net$p.val <- 1
      }# end if else (length(unique(betas_check))>1)
    } # end while(squared.jump_second.adapt.phase_net$p.val < 0.1)
  } # end if(length(unique(betas_check))>1)
  
  squared.jump_second.adapt.phase_net$constant <- constant
  squared.jump_second.adapt.phase_net$beta_sd <- second.adapt.phase_net$beta_sd
  B_prior_mat_weighted <- c(B_prior_mat)
  conditions <- c(B_prior_mat)==woPKGE_belief & c(squared.jump_second.adapt.phase_net$iter_edges[,,1])>0
  B_prior_mat_weighted[conditions] <- c(squared.jump_second.adapt.phase_net$iter_edges[,,2])[conditions] / c(squared.jump_second.adapt.phase_net$iter_edges[,,1])[conditions]
  squared.jump_second.adapt.phase_net$B_prior_mat_weighted <- matrix(B_prior_mat_weighted, nrow=nrow(B_prior_mat), dimnames = list(rownames(B_prior_mat), colnames(B_prior_mat)))
  squared.jump_second.adapt.phase_net$partition_func_UB <- pf_UB_est(omics = omics, layers_def = layers_def, B_prior_mat = squared.jump_second.adapt.phase_net$B_prior_mat_weighted, annot = annot)
  return(squared.jump_second.adapt.phase_net)
}
