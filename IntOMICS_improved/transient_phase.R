#' #######################
#' ### transient phase ###
#' ### to check if the chain is moving towards the mode of target distribution
#' @export
transient_phase <- function(first.adapt.phase_net, omics, B_prior_mat, layers_def, energy_all_configs_node, prob_mbr, BGe_score_all_configs_node, parent_set_combinations, annot, imprvd_data)
{
  beta_1st_adapt <- first.adapt.phase_net$betas[[length(first.adapt.phase_net$betas)]]$len
  source.net <- first.adapt.phase_net$nets[[length(first.adapt.phase_net$nets)]]
  beta.source <- first.adapt.phase_net$betas[[length(first.adapt.phase_net$betas)]]
  start <- length(first.adapt.phase_net$nets)


  NUM_TRANS_ITERS <- 200
  NUM_TRANS_POINTS <- 5
  MAX_ITER <- 12000

  # We employ a standard MCMC algorithm with proposals determined by the 1st adaption phase.
  # To check if the chain is moving towards the mode of target distribution, for every 200 iteration, the values generated for beta are averaged
  # With 5 different averages, a linear model is fitted to see if there is any trend in the beta coordinate of the chain.
  for(i in (start+1):(start+NUM_TRANS_ITERS*NUM_TRANS_POINTS))
  {
    ## method choice:
    first.adapt.phase_net$method_choice_saved[i] <- sample(x = c("MC3", "MBR"), size = 1, prob = c(1-prob_mbr, prob_mbr))
    if(first.adapt.phase_net$method_choice_saved[i]=="MC3")
    {
      ### candidate.net definition:
      candidate.net <- MC3(source_net = source.net,
                           layers_def =  layers_def,
                           B_prior_mat = B_prior_mat,
                           beta.source = beta.source,
                           partition_func_UB_beta_source = first.adapt.phase_net$partition_func_UB_beta_source,
                           omics = omics,
                           parent_set_combinations = parent_set_combinations,
                           BGe_score_all_configs_node = BGe_score_all_configs_node,
                           annot = annot,
                           imprvd_data=imprvd_data)
      ## Proposal distribution Q(..|..)
      # Q(source.net|candidate.net)
      # Q(candidate.net|source.net)
      candidate.net$proposal.distr <- log(1/source.net$nbhd.size)
      source.net$proposal.distr <- log(1/candidate.net$nbhd.size)
      candidate.net$likelihood <- candidate.net$likelihood_part + source.net$proposal.distr
      source.net$likelihood <- source.net$likelihood_part + candidate.net$proposal.distr
      first.adapt.phase_net$acceptance_saved[i] <- candidate.net$likelihood - source.net$likelihood

      # ?? candidate_edge <- which(candidate.net$adjacency!=source.net$adjacency, arr.ind = TRUE)

      u <- log(runif(1))
      if (u < first.adapt.phase_net$acceptance_saved[i])
      {
        source.net <- candidate.net
        beta.source$prior <- source.net$prior
      } # end if (u < first.adapt.phase_net$acceptance_saved[i])
      first.adapt.phase_net$nets[[i]] <- source.net

    } else {

      candidate.net <- MBR(source_net_adjacency = source.net$adjacency,
                           layers_def = layers_def,
                           omics = omics,
                           BGe_score_all_configs_node = BGe_score_all_configs_node,
                           parent_set_combinations = parent_set_combinations)

      nodes_cand <- rownames(candidate.net$adjacency)[which(regexpr("ENTREZ",rownames(candidate.net$adjacency))>0)]

      bge_per_node <- sapply(1:length(nodes_cand),
                             function(idx)
                             {
                                 nom <- nodes_cand[idx]
                                 bge_idx <- BGe_node(node=nom, adjacency_matrix = candidate.net$adjacency,
                                                     parent_set_combinations = NA, BGe_score_all_configs_node = NA,
                                                     imprvd_data = imprvd_data)
                                 bge_idx
                             })
      candidate.net$BGe_per_node <- bge_per_node
      #candidate.net$BGe <-BGe_score(adjacency_matrix = candidate.net$adjacency, omics = omics, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node, imprvd_data = imprvd_data)
      #candidate.net$BGe <-BGe_score(adjacency_matrix = candidate.net$adjacency, omics = omics, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node, imprvd_data=imprvd_data)
      # if(sum(bge_per_node) != candidate.net$BGe)
      # {
      #     asd <- 1
      #     stop("wrong implementation")
      # }
      candidate.net$BGe <- sum(bge_per_node)


      first.adapt.phase_net$acceptance_saved[i] <- candidate.net$acceptance
      ### source.net parameters necessary for MC3 method:
      ## Marginal likelihood P(D|G) using ln(BGe score):
      #candidate.net$BGe <-BGe_score(adjacency_matrix = candidate.net$adjacency, omics = omics, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node, imprvd_data = imprvd_data)
      candidate.net$nbhd.size <- neighborhood_size(net = candidate.net$adjacency, layers_def = layers_def, B_prior_mat = B_prior_mat, omics = omics)
      ## Prior probability
      candidate.net$energy <- sum(epsilon(net = candidate.net$adjacency, B_prior_mat = B_prior_mat))
      # partition_func_UB is already log transformed
      # the output of this function is also log transformed (natural logarithm)!
      candidate.net$prior <- (-beta.source$value*candidate.net$energy) - first.adapt.phase_net$partition_func_UB_beta_source
      candidate.net$likelihood_part <- candidate.net$BGe + candidate.net$prior

      u <- log(runif(1))
      if (u < first.adapt.phase_net$acceptance_saved[i])
      {
        source.net <- candidate.net
        beta.source$prior <- source.net$prior
      } # end if (u < acceptance_saved[i])
      first.adapt.phase_net$nets[[i]] <- source.net

      #partition_func_UB_beta_source <- sum(mapply(first.adapt.phase_net$energy_all_configs_node,FUN=function(x) logSumExp(-beta.source$value*x)))
      partition_func_UB_beta_source <- imprvd_log_UB_for_specific_beta(imprvd_data$hist, imprvd_data$coefs, beta.source$value)
    } # end if(method.choice=="MC3")

    ### MH for beta with fixed network structure ###
    beta.candidate <- list(value = rnorm(1, mean = beta.source$value, sd = beta_1st_adapt), prior = c(), len = beta_1st_adapt)
    if(beta.candidate$value < 0.5)
    {
      beta.candidate$value <- 0.5
    } # end if(beta.candidate$value < 0.5)

    #partition_func_UB_beta_candidate <- sum(mapply(first.adapt.phase_net$energy_all_configs_node,FUN=function(x) logSumExp(-beta.candidate$value*x)))
    partition_func_UB_beta_candidate <- imprvd_log_UB_for_specific_beta(imprvd_data$hist, imprvd_data$coefs, beta.candidate$value)
    beta.candidate$prior <- (-beta.candidate$value*source.net$energy) - partition_func_UB_beta_candidate

    first.adapt.phase_net$acceptance_beta_saved[i] <- beta.candidate$prior - beta.source$prior
    u_beta <- log(runif(1))

    if (u_beta < first.adapt.phase_net$acceptance_beta_saved[i])
    {
      beta.source <- beta.candidate
      first.adapt.phase_net$partition_func_UB_beta_source <- partition_func_UB_beta_candidate
    } # end if (u_beta < first.adapt.phase_net$acceptance_beta_saved[i])
    first.adapt.phase_net$betas[[i]] <- beta.source
  } # end for(i in (start+1):(start+1000))

  last_ten_pvals <- rep(0, 10)
  max_pval_all_time <- -10000000
  max_pval_iter <- -1

  p_iter <- 0




  beta_means <- colMeans(matrix(mapply(tail(first.adapt.phase_net$betas,NUM_TRANS_ITERS*NUM_TRANS_POINTS), FUN=function(list) list$value),nrow=NUM_TRANS_ITERS))
  reg_dat <- data.frame(beta_means = tail(beta_means,NUM_TRANS_POINTS), iter = 1:NUM_TRANS_POINTS)
  model <- lm(beta_means ~ iter, data = reg_dat)
  p.val <- summary(model)$coefficients[1,4]

  # We use a regression method to make sure the chain values are moving to only one direction, neither increasing nor deceasing.
  # If a regression method confirms that the chain values show a linear trend, we presume that the chain is still moving to a local mode.
  # The p-value for the slope coefficient is used to determine whether there is any linear trend.
  # If p-value for every coordinate is greater than 0.1, the chain gets stopped and this phase ends.
  while(p.val < 0.1)
  {
    for(i in (length(first.adapt.phase_net$nets)+1):(length(first.adapt.phase_net$nets)+NUM_TRANS_ITERS))
    {
      ## method choice:
      first.adapt.phase_net$method_choice_saved[i] <- sample(x = c("MC3", "MBR"), size = 1, prob = c(1-prob_mbr, prob_mbr))
      if(first.adapt.phase_net$method_choice_saved[i]=="MC3")
      {
        ### candidate.net definition:
        candidate.net <- MC3(source_net = source.net,
                             layers_def =  layers_def,
                             B_prior_mat = B_prior_mat,
                             beta.source = beta.source,
                             partition_func_UB_beta_source = first.adapt.phase_net$partition_func_UB_beta_source,
                             omics = omics,
                             parent_set_combinations = parent_set_combinations,
                             BGe_score_all_configs_node = BGe_score_all_configs_node,
                             annot = annot,
                             imprvd_data = imprvd_data)
        ## Proposal distribution Q(..|..)
        # Q(source.net|candidate.net)
        # Q(candidate.net|source.net)
        candidate.net$proposal.distr <- log(1/source.net$nbhd.size)
        source.net$proposal.distr <- log(1/candidate.net$nbhd.size)
        candidate.net$likelihood <- candidate.net$likelihood_part + source.net$proposal.distr
        source.net$likelihood <- source.net$likelihood_part + candidate.net$proposal.distr
        first.adapt.phase_net$acceptance_saved[i] <- candidate.net$likelihood - source.net$likelihood

        # ?? candidate_edge <- which(candidate.net$adjacency!=source.net$adjacency, arr.ind = TRUE)

        u <- log(runif(1))
        if (u < first.adapt.phase_net$acceptance_saved[i])
        {
          source.net <- candidate.net
          beta.source$prior <- source.net$prior
        } # end if (u < first.adapt.phase_net$acceptance_saved[i])
        first.adapt.phase_net$nets[[i]] <- source.net

      } else {

        candidate.net <- MBR(source_net_adjacency = source.net$adjacency,
                             layers_def = layers_def,
                             omics = omics,
                             BGe_score_all_configs_node = BGe_score_all_configs_node,
                             parent_set_combinations = parent_set_combinations)
        first.adapt.phase_net$acceptance_saved[i] <- candidate.net$acceptance
        ### source.net parameters necessary for MC3 method:
        ## Marginal likelihood P(D|G) using ln(BGe score):
        nodes_cand <- rownames(candidate.net$adjacency)[which(regexpr("ENTREZ",rownames(candidate.net$adjacency))>0)]

        bge_per_node <- sapply(1:length(nodes_cand),
                               function(idx)
                               {
                                   nom <- nodes_cand[idx]
                                   bge_idx <- BGe_node(node=nom, adjacency_matrix = candidate.net$adjacency,
                                                       parent_set_combinations = NA, BGe_score_all_configs_node = NA,
                                                       imprvd_data = imprvd_data)
                                   bge_idx
                               })
        candidate.net$BGe_per_node <- bge_per_node
        # candidate.net$BGe <-BGe_score(adjacency_matrix = candidate.net$adjacency, omics = omics, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node, imprvd_data = imprvd_data)
        # #candidate.net$BGe <-BGe_score(adjacency_matrix = candidate.net$adjacency, omics = omics, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node, imprvd_data=imprvd_data)
        # if(sum(bge_per_node) != candidate.net$BGe)
        # {
        #     asd <- 1
        #     stop("wrong implementation")
        # }
        candidate.net$BGe <- sum(bge_per_node)





        #candidate.net$BGe <-BGe_score(adjacency_matrix = candidate.net$adjacency, omics = omics, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node, imprvd_data = imprvd_data)
        candidate.net$nbhd.size <- neighborhood_size(net = candidate.net$adjacency, layers_def = layers_def, B_prior_mat = B_prior_mat, omics = omics)
        ## Prior probability
        candidate.net$energy <- sum(epsilon(net = candidate.net$adjacency, B_prior_mat = B_prior_mat))
        # partition_func_UB is already log transformed
        # the output of this function is also log transformed (natural logarithm)!
        candidate.net$prior <- (-beta.source$value*candidate.net$energy) - first.adapt.phase_net$partition_func_UB_beta_source
        candidate.net$likelihood_part <- candidate.net$BGe + candidate.net$prior

        u <- log(runif(1))
        if (u < first.adapt.phase_net$acceptance_saved[i])
        {
          source.net <- candidate.net
          beta.source$prior <- source.net$prior
        } # end if (u < acceptance_saved[i])
        first.adapt.phase_net$nets[[i]] <- source.net

        #partition_func_UB_beta_source <- sum(mapply(first.adapt.phase_net$energy_all_configs_node,FUN=function(x) logSumExp(-beta.source$value*x)))
        partition_func_UB_beta_source <- imprvd_log_UB_for_specific_beta(imprvd_data$hist, imprvd_data$coefs, beta.source$value)

      } # end if(method.choice=="MC3")

      ### MH for beta with fixed network structure ###
      beta.candidate <- list(value = rnorm(1, mean = beta.source$value, sd = beta_1st_adapt), prior = c(), len = beta_1st_adapt)
      if(beta.candidate$value < 0.5)
      {
        beta.candidate$value <- 0.5
      } # end if(beta.candidate$value < 0.5)

      #partition_func_UB_beta_candidate <- sum(mapply(first.adapt.phase_net$energy_all_configs_node,FUN=function(x) logSumExp(-beta.candidate$value*x)))
      partition_func_UB_beta_candidate <- imprvd_log_UB_for_specific_beta(imprvd_data$hist, imprvd_data$coefs, beta.candidate$value)


      beta.candidate$prior <- (-beta.candidate$value*source.net$energy) - partition_func_UB_beta_candidate

      first.adapt.phase_net$acceptance_beta_saved[i] <- beta.candidate$prior - beta.source$prior
      u_beta <- log(runif(1))

      if (u_beta < first.adapt.phase_net$acceptance_beta_saved[i])
      {
        beta.source <- beta.candidate
        first.adapt.phase_net$partition_func_UB_beta_source <- partition_func_UB_beta_candidate
      } # end if (u_beta < first.adapt.phase_net$acceptance_beta_saved[i])
      first.adapt.phase_net$betas[[i]] <- beta.source
    } # end for(i in (length(first.adapt.phase_net$nets)+1):(length(first.adapt.phase_net$nets)+200))

    beta_means <- colMeans(matrix(mapply(tail(first.adapt.phase_net$betas, NUM_TRANS_POINTS*NUM_TRANS_ITERS), FUN=function(list) list$value),nrow=NUM_TRANS_ITERS))
    reg_dat <- data.frame(beta_means = beta_means, iter = 1:NUM_TRANS_POINTS)

    model <- lm(beta_means ~ iter, data = reg_dat)
    p.val <- summary(model)$coefficients[1,4]

    # store data
    last_ten_pvals <- last_ten_pvals[2:10]
    last_ten_pvals <- c(last_ten_pvals, p.val)
    if(max_pval_all_time < p.val)
    {
        max_pval_all_time <- p.val
        max_pval_iter <- p_iter
    }

    if(!is.null(dev.list())) dev.off()
    par(mfrow=c(1,2))

    plot(beta_means ~ iter, data = reg_dat, ask=FALSE, main=sprintf("P.val = %7.6f", p.val), ylim=c(0,1))
    abline(model)

    plot(1:10, -log10(last_ten_pvals), main="Last 10 p-values", ylim = c(0, 10))
    lines(1:10, -log10(last_ten_pvals), pch=16, ylim = c(0, 10))
    abline(h=-log10(0.1), col="red")
    text(3.5, -log10(0.1),"0.1000",srt=0.2,pos=3)

    abline(h=-log10(max_pval_all_time), col="green")
    text(3.5, -log10(max_pval_all_time),
         sprintf("%5.4f (iter: %d)", max_pval_all_time, max_pval_iter),
         srt=0.2,pos=3)

    p_iter <- p_iter + 1
    printf("%s / %d \r", p_iter, MAX_ITER)


    if(p_iter > MAX_ITER)
    {
        #stop("Too many iterations, should repeat!")
        return(NA)
    }


  } # end while(p.val < 0.1)
  printf("Total iterations: %d", p_iter)
  printf("%5.4f (iter: %d)", max_pval_all_time, max_pval_iter)
  return(first.adapt.phase_net)
}
