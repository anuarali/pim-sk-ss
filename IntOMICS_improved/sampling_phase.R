#' ######################
#' ### sampling phase ###
#' ### Now we apply 2 MCMC simulations and check the RMS value (https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0734-6) to stop the simulation
#' ### After the burn-in period, we discard the values from the first half of this phase
#' @export
sampling_phase <- function(second.adapt.phase_net, seed1, seed2, omics, layers_def, prob_mbr, thin, minseglen, burn_in, annot) 
{
  init.net_sampling <- init.net.mcmc(omics = omics, seed = seed1, layers_def = layers_def, B_prior_mat = second.adapt.phase_net$B_prior_mat_weighted)
  init.net_sampling <- source_net_def(init.net.mcmc.output = init.net_sampling, 
                                      omics = omics, 
                                      parent_set_combinations = second.adapt.phase_net$partition_func_UB$parents_set_combinations,
                                      BGe_score_all_configs_node = second.adapt.phase_net$partition_func_UB$BGe_score_all_configs_node,
                                      B_prior_mat = second.adapt.phase_net$B_prior_mat_weighted,
                                      layers_def = layers_def,
                                      energy_all_configs_node = second.adapt.phase_net$partition_func_UB$energy_all_configs_node,
                                      len = tail(second.adapt.phase_net$betas,1)[[1]][["len"]])
  init.net_sampling$seed <- seed1
  
  rms <- c()
  seeds_res <- list(seed1=list(),seed2=list())
  # seed2 network is random
  seeds_res$seed1$nets <- tail(second.adapt.phase_net$nets,1)
  seeds_res$seed1$betas <- tail(second.adapt.phase_net$betas,1)
  seeds_res$seed1$partition_func_UB_beta_source <- second.adapt.phase_net$partition_func_UB_beta_source
  seeds_res$seed1$nets[[1]]$adjacency <- init.net_sampling$source.net$adjacency
  seeds_res$seed1$nets[[1]]$nbhd.size <- neighborhood_size(net = seeds_res$seed1$nets[[1]]$adjacency, layers_def = layers_def, B_prior_mat = second.adapt.phase_net$B_prior_mat_weighted, omics = omics)
  seeds_res$seed1$nets[[1]]$energy <- sum(epsilon(net = seeds_res$seed1$nets[[1]]$adjacency, B_prior_mat = second.adapt.phase_net$B_prior_mat_weighted))
  seeds_res$seed1$nets[[1]]$prior <- (-seeds_res$seed1$betas[[1]]$value*seeds_res$seed1$nets[[1]]$energy) - seeds_res$seed1$partition_func_UB_beta_source
  seeds_res$seed1$nets[[1]]$BGe <- BGe_score(adjacency_matrix = seeds_res$seed1$nets[[1]]$adjacency, omics = omics, layers_def = layers_def, parent_set_combinations = second.adapt.phase_net$partition_func_UB$parents_set_combinations, BGe_score_all_configs_node = second.adapt.phase_net$partition_func_UB$BGe_score_all_configs_node)
  seeds_res$seed1$nets[[1]]$likelihood_part <- seeds_res$seed1$nets[[1]]$BGe + seeds_res$seed1$nets[[1]]$prior
  
  seeds_res$seed1$betas[[1]]$prior <- seeds_res$seed1$nets[[1]]$prior
  seeds_res$seed1$seed <- seed1
  seeds_res$seed1$nets[[1]]$proposal.distr <- c()
  
  seeds_res$seed1$acceptance_saved <- vector("numeric")
  seeds_res$seed1$method_choice_saved <- vector("numeric")
  seeds_res$seed1$layers <- second.adapt.phase_net$layers
  seeds_res$seed1$seed <- seed1
  seeds_res$seed1$cpdags <- list()
  
  # seed2 network is the empty
  seeds_res$seed2 <- seeds_res$seed1
  seeds_res$seed2$nets[[1]]$adjacency[seeds_res$seed2$nets[[1]]$adjacency==1] <- 0
  seeds_res$seed2$nets[[1]]$nbhd.size <- neighborhood_size(net = seeds_res$seed2$nets[[1]]$adjacency, layers_def = layers_def, B_prior_mat = second.adapt.phase_net$B_prior_mat_weighted, omics = omics)
  seeds_res$seed2$nets[[1]]$energy <- sum(epsilon(net = seeds_res$seed2$nets[[1]]$adjacency, B_prior_mat = second.adapt.phase_net$B_prior_mat_weighted))
  seeds_res$seed2$nets[[1]]$prior <- (-seeds_res$seed2$betas[[1]]$value*seeds_res$seed2$nets[[1]]$energy) - seeds_res$seed2$partition_func_UB_beta_source
  seeds_res$seed2$nets[[1]]$BGe <- BGe_score(adjacency_matrix = seeds_res$seed2$nets[[1]]$adjacency, omics = omics, layers_def = layers_def, parent_set_combinations = second.adapt.phase_net$partition_func_UB$parents_set_combinations, BGe_score_all_configs_node = second.adapt.phase_net$partition_func_UB$BGe_score_all_configs_node)
  seeds_res$seed2$nets[[1]]$likelihood_part <- seeds_res$seed2$nets[[1]]$BGe + seeds_res$seed2$nets[[1]]$prior
  seeds_res$seed2$betas[[1]]$prior <- seeds_res$seed2$nets[[1]]$prior
  seeds_res$seed2$seed <- seed2

  counting <- 0
  mcmc_sim_part_res <- lapply(seeds_res, 
                              FUN=function(list_l) mcmc.simulation_sampling.phase(first = 1,
                                                                                  last = thin,
                                                                                  sim_init = list_l,
                                                                                  prob_mbr = prob_mbr,
                                                                                  B_prior_mat = second.adapt.phase_net$B_prior_mat_weighted,
                                                                                  omics = omics,
                                                                                  parent_set_combinations = second.adapt.phase_net$partition_func_UB$parents_set_combinations,
                                                                                  BGe_score_all_configs_node = second.adapt.phase_net$partition_func_UB$BGe_score_all_configs_node,
                                                                                  layers_def = layers_def,
                                                                                  len = seeds_res$seed1$betas[[1]]$len, 
                                                                                  counting = counting,
                                                                                  thin = thin,
                                                                                  energy_all_configs_node = second.adapt.phase_net$partition_func_UB$energy_all_configs_node,
                                                                                  annot = annot))
  
  # remove duplicated cpdags
  cpdags1 <- mcmc_sim_part_res$seed1$cpdags
  cpdags2 <- mcmc_sim_part_res$seed2$cpdags
  cpdag_weights1 <- custom.strength(cpdags1, nodes = bnlearn::nodes(cpdags1[[1]]), weights = NULL)
  cpdag_weights2 <- custom.strength(cpdags2, nodes = bnlearn::nodes(cpdags2[[1]]), weights = NULL)
  cpdag_weights1 <- cpdag_weights1[cpdag_weights1$direction>=0.5,]
  cpdag_weights2 <- cpdag_weights2[cpdag_weights2$direction>=0.5,]
  total <- merge(cpdag_weights1, cpdag_weights2, by = c("from","to"))
  N <- nrow(total)
  dist_i <- abs(total$strength.x - total$strength.y)^2 / 2
  rms <- c(rms,sqrt(1/N*sum(dist_i)))
 
  while(length(mcmc_sim_part_res$seed1$nets)<(2*burn_in))
  {
    counting <- counting+1
    mcmc_sim_part_res <- lapply(mcmc_sim_part_res, 
                                FUN=function(list_l) mcmc.simulation_sampling.phase(first = length(list_l$nets)+1,
                                                                          last = length(list_l$nets)+thin,
                                                                          sim_init = list_l, 
                                                                          prob_mbr = prob_mbr,
                                                                          B_prior_mat = second.adapt.phase_net$B_prior_mat_weighted,
                                                                          omics = omics,
                                                                          parent_set_combinations = second.adapt.phase_net$partition_func_UB$parents_set_combinations,
                                                                          BGe_score_all_configs_node = second.adapt.phase_net$partition_func_UB$BGe_score_all_configs_node,
                                                                          layers_def = layers_def,
                                                                          len = seeds_res$seed1$betas[[1]]$len, 
                                                                          counting = counting,
                                                                          thin = thin,
                                                                          energy_all_configs_node = second.adapt.phase_net$partition_func_UB$energy_all_configs_node,
                                                                          annot = annot))
    # remove duplicated cpdags
    cpdags1 <- unique(mcmc_sim_part_res$seed1$cpdags)
    cpdags2 <- unique(mcmc_sim_part_res$seed2$cpdags)
    cpdag_weights1 <- custom.strength(cpdags1, nodes = bnlearn::nodes(cpdags1[[1]]), weights = NULL)
    cpdag_weights2 <- custom.strength(cpdags2, nodes = bnlearn::nodes(cpdags2[[1]]), weights = NULL)
    cpdag_weights1 <- cpdag_weights1[cpdag_weights1$direction>=0.5,]
    cpdag_weights2 <- cpdag_weights2[cpdag_weights2$direction>=0.5,]
    total <- merge(cpdag_weights1, cpdag_weights2, by = c("from","to"))
    N <- nrow(total)
    dist_i <- abs(total$strength.x - total$strength.y)^2 / 2
    rms <- c(rms,sqrt(1/N*sum(dist_i)))
  } # end while(length(mcmc_sim_part_res$seed1$nets)<(2*burn_in))
  rms_strength <- abs(diff(rms))
  strength_threshold <- quantile(rms_strength, 0.75, na.rm = TRUE) # 3rd quartile of RMS strength is the threshold
  
  while(any(tail(rms_strength,minseglen/thin)>strength_threshold))
  {
    counting <- counting + 1
    mcmc_sim_part_res <- lapply(mcmc_sim_part_res, 
                                FUN=function(list_l) mcmc.simulation_sampling.phase(first = length(list_l$nets)+1,
                                                                          last = length(list_l$nets)+thin,
                                                                          sim_init = list_l, 
                                                                          prob_mbr = prob_mbr,
                                                                          B_prior_mat = second.adapt.phase_net$B_prior_mat_weighted,
                                                                          omics = omics,
                                                                          parent_set_combinations = second.adapt.phase_net$partition_func_UB$parents_set_combinations,
                                                                          BGe_score_all_configs_node = second.adapt.phase_net$partition_func_UB$BGe_score_all_configs_node,
                                                                          layers_def = layers_def,
                                                                          len = seeds_res$seed1$betas[[1]]$len,
                                                                          counting = counting,
                                                                          thin = thin,
                                                                          energy_all_configs_node = second.adapt.phase_net$partition_func_UB$energy_all_configs_node,
                                                                          annot = annot))
    # remove duplicated cpdags
    cpdags1 <- unique(mcmc_sim_part_res$seed1$cpdags)
    cpdags2 <- unique(mcmc_sim_part_res$seed2$cpdags)
    cpdag_weights1 <- custom.strength(cpdags1, nodes = bnlearn::nodes(cpdags1[[1]]), weights = NULL)
    cpdag_weights2 <- custom.strength(cpdags2, nodes = bnlearn::nodes(cpdags2[[1]]), weights = NULL)
    cpdag_weights1 <- cpdag_weights1[cpdag_weights1$direction>=0.5,]
    cpdag_weights2 <- cpdag_weights2[cpdag_weights2$direction>=0.5,]
    total <- merge(cpdag_weights1, cpdag_weights2, by = c("from","to"))
    N <- nrow(total)
    dist_i <- abs(total$strength.x - total$strength.y)^2 / 2
    rms <- c(rms,sqrt(1/N*sum(dist_i)))
    rms_strength <- abs(diff(rms))
  } # end while(any(tail(rms_strength,minseglen/thin)>strength_threshold))
  
  return(list(mcmc_sim_part_res = mcmc_sim_part_res, rms = rms))
}
