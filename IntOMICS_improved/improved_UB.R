###############################################################################################
## This function computes for each GE/CNV node its energy over all parent set configurations ##
## Also empty parent set is included                                                         ##
###############################################################################################
# int_node is character, the name of given node
# all_parents_config is matrix, each column indicates parents of given int_node


imprvd_energy_function_node_specific <- function(all_parents_config, B_prior_mat, int_node)
{
    epsilon <- apply(all_parents_config,2,FUN=function(z)
        if(is.na(z[1]))
        {
            sum(B_prior_mat[,int_node])
        } else {
            sum(1-B_prior_mat[z,int_node]) + sum(B_prior_mat[-match(z,rownames(B_prior_mat)),int_node])
        })
    return(epsilon)
}


imprvd_coefs_matrix <- function(TFBS_belief = 0.75, nonGE_belief = 0.5, woPKGE_belief = 0.5, absent_belief=0, GE_belief=1)
{
    coefs <- data.frame(matrix(0, 1, 5))
    colnames(coefs) <- c("absent", "noPK", "GE", "nonGE", "TF")

    coefs$absent <- absent_belief
    coefs$GE <- GE_belief
    coefs$TF <- TFBS_belief
    coefs$nonGE <- nonGE_belief
    coefs$noPK <- woPKGE_belief

    return(coefs)
}


imprvd_calculate_histogram_of_B_prior <- function(B_prior_mat, coefs)
{
    library(dplyr)

    N <- nrow(B_prior_mat)

    imprvd_get_counts_per_node <- function(col_B, coefs)
    {
        nums <- match(col_B, coefs[1,])  # get indices for every element
        counts <- as.data.frame(table(nums))

        ret <- data.frame(matrix(0, 1, 5))
        colnames(ret) <- colnames(coefs)
        ret[,as.numeric(levels(counts$nums))] <- counts$Freq

        return(ret)
    }

    nums <- do.call("rbind", apply(B_prior_mat, 2, imprvd_get_counts_per_node, coefs=coefs))
    H <- suppressMessages(
        nums %>% group_by_all() %>% summarise(hist_count = n())
        )

    H <- data.frame(H)

    return(H)
}


..imprvd_calculation_log_per_type <- function(combo, beta)
{
    N <- combo[1]
    C <- combo[2]

    if(C == 0 || N == 0)
    {
        return(0)
    }

    if(N <= 3)
    {
        K <- 0:N
    }
    else{
        K <- 0:3
    }

    choose_term <- choose(N, K)

    term_exp <- N * C  + (1 - 2 * C) * K
    term_exp <- -beta * term_exp
    ret <- sum(choose_term * exp(term_exp))
    ret <- log(ret)

    return(ret)
}

..imprvd_calculation_per_set <- function(row_hist, coefs, beta)
{
    extended_row <- data.frame(matrix(0, nrow=2, ncol=length(row_hist)))
    extended_row[1, 1:length(row_hist)] <- row_hist
    extended_row[2, 1:ncol(coefs)] <- coefs
    count <- row_hist[["hist_count"]]


    res <- apply(extended_row[,1:ncol(coefs)], 2, ..imprvd_calculation_log_per_type, beta=beta)
    res <- sum(res)

    return(res * count)
}

imprvd_log_UB_for_specific_beta <- function(histogram, coefs, beta, max_fan_in=3)
{
    ret <- apply(histogram, 1, ..imprvd_calculation_per_set, coefs=coefs, beta=beta)
    ret <- sum(ret)
    return(ret)
}

printf <- function(...) cat(sprintf(...))

imprvd_sample_init_net <- function(empty_net, seed, B_prior)
{
    if(!missing(seed))
    {
        set.seed(seed)
    }

    is_acyc <- FALSE
    ret_net <- NA

    while(!is_acyc)
    {
        # start by picking the random parent for each node
        pick_random_par_per_node <- function(idx_col)
        {
            col <- B_prior[,idx_col]
            allowed_in_par <- (1:ncol(B_prior))[col != 0]  # take all allowed parents
            ret <- rep(0, length(col))

            if(length(allowed_in_par) > 0)
            {
                picked <- allowed_in_par[sample(length(allowed_in_par), 1)]  # pick 1 of them randomly

                if(runif(1) > 0.25)#0.5
                {
                    ret[picked] <- 1
                }
            }

            return(ret)
        }
        ret_net <- sapply(1:ncol(B_prior), pick_random_par_per_node)
        is_acyc <- is.acyclic(ret_net) # if contains cycles -> repeat
    }

    dataset_BND <- BNDataset(data = empty_net,
                             discreteness = rep('d',ncol(empty_net)),
                             variables = c(colnames(empty_net)),
                             node.sizes = rep(2,ncol(empty_net)), starts.from=0)
    net <- BN(dataset_BND)
    dag(net) <- ret_net
    return( suppressMessages(learn.params(net,dataset_BND)) )
}

imprvd_parent_sets_sum_scores_X <- function(selected_node, descendants, parent_set, B_prior)
{
    # take all possible in nodes
    all_allowed_in_parents <- names(which(B_prior[,selected_node] > 0))
    
    # remove current children from them
    all_allowed_in_parents <- all_allowed_in_parents[!(all_allowed_in_parents %in% descendants)]
    
    # remove one parent uniformly
    if(!all(is.na(parent_set)))  # only if not NA
    {
      all_allowed_in_parents <- all_allowed_in_parents[all_allowed_in_parents != sample(parent_set, 1)]
    }
    
    
    # choose how many parents needed
    K <- length(all_allowed_in_parents)
    K_need <- sample(0:K, 1)
    
    # sample those 
    sampled_parents <- sample(all_allowed_in_parents, K_need)
    
    #return
    ret <- list(new_parent_set = sampled_parents)
    
    if(length(sampled_parents) == 0)
    {
      ret$new_parent_set <- NA
    }
    #, sum_score_unmarked = 0)
    #else{
    #  adj_synt <- matrix(0, ncol(B_prior), 1)
    #  colnames(adj_synt) <- selected_node
    #  rownames(adj_synt) <- colnames(B_prior)
    #  adj_synt[sampled_parents,selected_node] <- 1
    #  ret$sum_score_unmarked <- BGe_node(selected_node, adj_synt, NA, NA, imprvd_data = imprvd_data)
    #}
    
    return(ret)
}

imprvd_parent_sets_sum_scores_childrenX <- function(selected_node, children_selected_node, child_order, dag_tmp_bn, B_prior)
{
  for(j in child_order)
  {
    descendants <- bnlearn::descendants(x = dag_tmp_bn, node = children_selected_node[j])
    
    all_allowed_in_parents <- names(which(B_prior[,children_selected_node[j]] > 0))
    
    # remove current children from them
    all_allowed_in_parents <- all_allowed_in_parents[!(all_allowed_in_parents %in% descendants)]  
    
    # remove current node
    all_allowed_in_parents <- all_allowed_in_parents[all_allowed_in_parents != selected_node]
    
    # choose how many parents needed
    K <- length(all_allowed_in_parents)
    K_need <- sample(0:K, 1)
    
    # sample those 
    sampled_parents <- sample(all_allowed_in_parents, K_need)
    
    # add selected node
    sampled_parents <- c(sampled_parents, selected_node)
    
    # update adjacency
    amat(dag_tmp_bn)[sampled_parents, children_selected_node[j]] <- 1
  }
  return(list(dag_tmp_bn = dag_tmp_bn))
}






circleFun <- function(center = c(0,0),diameter = 1, npoints = 100){
    r = diameter / 2
    tt <- seq(0,2*pi,length.out = npoints)
    xx <- center[1] + r * cos(tt)
    yy <- center[2] + r * sin(tt)
    return(data.frame(x = xx, y = yy))
}


determineType <- function(idx, B_prior)
{
    in_deg <- sum(B_prior[,idx] > 0)
    in_deg <- in_deg > 0
    out_deg <- sum(B_prior[idx,] > 0)
    out_deg <- out_deg > 0

    # Just a boolean logic for faster computation
    # IN = 0 and OUT > 0 ----> circ = 1
    # IN > 0 and OUT > 0 ----> miRNA = 2
    # IN > 0 and OUT = 0 ----> mRNA = 3
    return(2 * (in_deg) * xor(in_deg, out_deg) + in_deg + out_deg)
}

determineYCoordinate <- function(vertex_order)
{
    nums <- colMaxs(vertex_order) + 1
    N_max <- max(vertex_order)
    coords <- t(t(vertex_order) / nums * N_max)
    coords <- rowSums(coords)
    return(coords)
}

extractAllEdges <- function(idx, I_incidence, B_prior, coords)
{
    out_edges <- I_incidence[idx,]
    out_edges <- which(as.logical(out_edges))

    prior_types <- B_prior[idx, out_edges]
    prior_types <- prior_types == 0.5
    prior_types <- ifelse(prior_types, "non-prior edge", "prior edge")


    K <- length(out_edges)
    if(K == 0)
    {
        return(NA)
    }
    my_coord <- coords[idx,]
    out_edges <- coords[out_edges,]

    sgm = data.frame(
        x = rep(as.numeric(my_coord[1]),K),
        xend = out_edges[,1],
        y = rep(as.numeric(my_coord[2]),K),
        yend = out_edges[,2],
        line_color=prior_types
    )

    return(sgm)
}


draw_entire_interation_network <- function(I_incidence, B_prior, beta=0, last_beta_move="initial beta", last_move="None: Initial graph", beta_sd=1, GRAPH_SIZE_FACTOR=20)
{
    N <- nrow(B_prior)

    if(last_move == "MBR")
    {
        last_move <- "Markov blanket resampling"
    }

    vertex_types <- sapply(1:N, function(idx) determineType(idx, B_prior = B_prior))
    vertex_order <- sapply(1:3, function(idx) cumsum(vertex_types == idx) * (vertex_types == idx))
    coords <<- data.frame(x = (vertex_types-1) * GRAPH_SIZE_FACTOR, y = determineYCoordinate(vertex_order))
    p <- ggplot()
    p <- p + ggtitle(paste("Last conducted move:\n", last_move, "\n", "Beta value:", beta, "(", last_beta_move, ")\n", "beta sd: ", beta_sd, sep=""))

    # Draw lines representation of interaction edges
    all_edges <- sapply(1:N, function(idx) extractAllEdges(idx, I_incidence = I_incidence, B_prior = B_prior, coords=coords))
    all_edges <- all_edges[!is.na(all_edges)]
    all_edges <- bind_rows(all_edges)

    p <- p + geom_segment(data=all_edges, mapping=aes(x = x, y = y, xend = xend, yend = yend, linetype=line_color), colour="black")
    p <- p + scale_linetype_manual(
            values = c("non-prior edge" = "longdash", "prior edge" = "solid"),
            name="Interaction edge type"
        )

    # Draw points and the text
    all_ids <- 1:N
    rna_names <- c("circRNA", "miRNA", "mRNA")

    p <- p + geom_point(data=coords, mapping=aes(x,y, color=as.factor(rna_names[vertex_types])), size=10)
    p <- p + xlim(0, 2 * GRAPH_SIZE_FACTOR + 5)
    p <- p + geom_text(data=coords, mapping=aes(x,y, color=as.factor(rna_names[vertex_types])),
                       label = ifelse(vertex_types != 3, all_ids, ""), nudge_y = -1, colour="black")
    p <- p + geom_text(data=coords, mapping=aes(x,y, color=as.factor(rna_names[vertex_types])),
                       label = ifelse(vertex_types == 3, all_ids, ""), nudge_x = 3, colour="black")
    p <- p + scale_color_manual(
        values = list("circRNA" = "white", "miRNA" = "green", "mRNA" = "blue"),
        name = "RNA type")



    return(p)

    segment_data = data.frame(
        x = c(1, 5),
        xend = c(1, 5),
        y = c(20, 30),
        yend = c(30, 40),
        line_color=c("prior edge", "non-prior edge")
    )
    #p <- p + ggplot(data = segment_data, aes(x = x, y = y, xend = xend, yend = yend, colour=line_color))
    p <- p + new_scale_color() + scale_color_manual(
        values = list("prior edge" = "red", "non-prior edge" = "green"),
        name = "Edge type")

    p <- p + geom_segment(data = segment_data, aes(x = x, y = y, xend = xend, yend = yend, colour=line_color))

    return(p)
}



gridLayout <- function(N)
{
    W <- ceiling(sqrt(N))
    H <- ceiling(N / W)
    x_row <- 1:W / W

    X <- rep(x_row, H)
    Y <- rep(1:H / H, each=W)
    coords <- matrix(c(X, Y), ncol=2)
    coords <- coords[1:N,]

    return(coords)
}

# Draw the structure space
#
# target_struct:
#               NA ---- just color the source
#               target_struct ----- color
draw_structure_space <- function(all_parent_set_combos, source_struct, target_struct=NA)
{
    L <- length(unlist(all_parent_set_combos))

    printf("Size is %d\n", L)
    adjmat <- matrix(0, L, L)
    colnames(adjmat) <- 1:L
    rownames(adjmat) <- 1:L

    #adjmat[sample(L, 1), sample(L, 1)] <- 1

    G <- graph_from_adjacency_matrix(
        adjmat
    )

    V(G)$color <- rep("red", L)
    V(G)$color[45] <- "green"
    V(G)$vsize <- rep(3, L)
    #V(G)$vsize[45] <- 10

    coords <- gridLayout(L)

    plot(G, layout = coords,
         vertex.size = V(G)$vsize,
         vertex.color=V(G)$color,
         vertex.frame.color= "white",
         vertex.label.color = "white",
         vertex.label.family = "sans",
         edge.width=2,
         edge.color="black",
         main="Structure space")

    #legend("topleft",bty = "n",
    #       legend=levels(Group),
    #       fill=pal, border=NA)
}




# A which for multidimensional arrays.
# Mark van der Loo 16.09.2011
#
# A Array of booleans
# returns a sum(A) x length(dim(A)) array of multi-indices where A == TRUE
#
# Taken from:
# https://www.r-bloggers.com/2011/09/a-multidimensional-which-function/
multi.which <- function(A){
    if ( is.vector(A) ) return(which(A))
    d <- dim(A)
    T <- which(A) - 1
    nd <- length(d)
    t( sapply(T, function(t){
        I <- integer(nd)
        I[1] <- t %% d[1]
        sapply(2:nd, function(j){
            I[j] <<- (t %/% prod(d[1:(j-1)])) %% d[j]
        })
        I
    }) + 1 )
}






